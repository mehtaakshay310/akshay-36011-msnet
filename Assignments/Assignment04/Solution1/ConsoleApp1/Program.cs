﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMSLib;

namespace ConsoleApp1
{
    class Program
    {
        static List<Employee> employees = new List<Employee>();
        static List<WageEmp> wageEmps = new List<WageEmp>();
        static List<Salesperson> salespersons = new List<Salesperson>();
        static List<Department> departments = new List<Department>();
        static void Main(string[] args)
        {
            string isContinue = "y";
            do
            {
                Console.WriteLine("Select operation to be performed");
                Console.WriteLine("1:Add Employee,2: View Employee Details, 3:Department");
                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1: 
                        addEmployee();
                        break;
                    case 2:
                        viewEmployee();
                        break;
                    case 3:
                        Department();
                        break;
                    default:
                        Console.WriteLine("Select Appropriate option");
                        break;
                }
                Console.WriteLine("Do you wish to continue(y/n)");
                isContinue = Console.ReadLine();
            } while (isContinue=="y");
        }

        private static void Department()
        {
            Console.WriteLine("Select Operation to be performed");
            Console.WriteLine("1:Add Department ,2: View Department");
            int choice = Convert.ToInt32(Console.ReadLine());
            try
            {
                switch (choice)
                {
                    case 1:
                        addDepartment();
                        break;
                    case 2:
                        viewDepartment();
                        break;
                    default:
                        Console.WriteLine("Select Appropriate option");
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Do you want to exit department section(y/n)");
            if (Console.ReadLine() == "n")
            {
                Department();
            }
        }

        private static void viewDepartment()
        {
            foreach (Department department in departments)
            {
                Console.WriteLine(department);
            }
        }

        private static void addDepartment()
        {
            Console.WriteLine("Enter Deaprtment Number:");
            int deptNo = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Department Name");
            string deptName = Console.ReadLine();

            Console.WriteLine("Enter Location of department");
            string location = Console.ReadLine();

            Department department = new Department(deptNo, deptName, location);

            departments.Add(department);
        }

        private static void viewEmployee()
        {
            Console.WriteLine("Select Employee to be Added");
            Console.WriteLine("1: View SalesPerson ,2: View Wage Employee, 3: Other Employee");
            int choice = Convert.ToInt32(Console.ReadLine());
           
            switch (choice)
            {
                case 1:
                    foreach (Employee employee in employees)
                    {
                        Console.WriteLine(employee);
                        Console.WriteLine(employee.getNetSalary());
                    }
                    break;
                case 2:
                    foreach (WageEmp employee in wageEmps)
                    {
                        Console.WriteLine(employee);
                        Console.WriteLine(employee.getNetSalary());
                    }
                    break;
                case 3:
                    foreach (Salesperson employee in salespersons)
                    {
                        Console.WriteLine(employee);
                        Console.WriteLine(employee.getNetSalary());
                    }
                    break;
                default:
                    Console.WriteLine("Select Appropriate option");
                    break;
            }
            
        }

        private static void addEmployee()
        {
            
            Console.WriteLine("Select Employee to be Added");
            Console.WriteLine("1:Salesperson ,2: Wageemp, 3: Other Employee");
            int choice = Convert.ToInt32(Console.ReadLine());
            try
            {
                switch (choice)
                {
                    case 1:
                        addSaleperson();
                        break;
                    case 2:
                        addWageemp();
                        break;
                    case 3:
                        addOtherEmployee();
                        break;
                    default:
                        Console.WriteLine("Select Appropriate option");
                        break;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Do you want to add more Employee(y/n)");
            if (Console.ReadLine() == "y")
            {
                addEmployee();
            }
        }

        private static void addOtherEmployee()
        {
            Console.WriteLine("Enter Name");
            string Name = Console.ReadLine();

            Console.WriteLine("Enter Your gender?(m/f)");
            Boolean Gender = Console.ReadLine() == "m" ? true : false;

            Date birth = new Date();
            Console.WriteLine("Enter year of birth");
            birth.Year = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Month of birth");
            birth.Month = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Day of birth");
            birth.Day = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Address");
            string Address = Console.ReadLine();

            Console.WriteLine("Enter salary");
            double Salary = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Enter designation");
            string Designation =Console.ReadLine();

            Console.WriteLine("Select Employee \n Type 0:Trainee, 1:Permanent, 2:Temporary");
            EmployeeTypes EmpType = (EmployeeTypes)Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Email ID");
            string email_id = Console.ReadLine();

            Console.WriteLine("Enter Passcode");
            string passcode = Console.ReadLine();

            Console.WriteLine("Enter Department Number from bellow");
            viewDepartment();
            int dept_no = Convert.ToInt32(Console.ReadLine());


            Date hiredate = new Date();
            Console.WriteLine("Enter year of hire Date");
            hiredate.Year = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Month of Hire Date");
            hiredate.Month = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Day of hire date");
            hiredate.Day = Convert.ToInt32(Console.ReadLine());

            Employee we = new Employee(Name, Address, birth, Gender,Salary,Designation,EmpType,email_id,passcode,dept_no,hiredate);
            employees.Add(we);
        }

        private static void addWageemp()
        {
            Console.WriteLine("Enter Name");
            string Name = Console.ReadLine();

            Console.WriteLine("Enter Your gender?(m/f)");
            Boolean Gender = Console.ReadLine() == "m" ? true : false;

            Date birth = new Date();
            Console.WriteLine("Enter year of birth");
            birth.Year = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Month of birth");
            birth.Month = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Day of birth");
            birth.Day = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Address");
            string Address = Console.ReadLine();

            Console.WriteLine("Select Employee \n Type 0:Trainee, 1:Permanent, 2:Temporary");
            EmployeeTypes EmpType = (EmployeeTypes)Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Hours");
            int hours = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Rates");
            int Rates = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Email ID");
            string email_id = Console.ReadLine();

            Console.WriteLine("Enter Passcode");
            string passcode = Console.ReadLine();

            Console.WriteLine("Enter Department Number from bellow");
            viewDepartment();
            int dept_no = Convert.ToInt32(Console.ReadLine());

            Date hiredate = new Date();
            Console.WriteLine("Enter year of hire Date");
            hiredate.Year = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Month of hire Date");
            hiredate.Month = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Day of hire Date");
            hiredate.Day = Convert.ToInt32(Console.ReadLine());

            WageEmp we = new WageEmp(Name, Address, birth, Gender, 0, EmpType, hours, Rates,email_id,passcode,dept_no,hiredate);
            wageEmps.Add(we);
        }

        private static void addSaleperson()
        {
            Console.WriteLine("Enter Name");
            string Name = Console.ReadLine();

            Console.WriteLine("Enter Your gender?(m/f)");
            Boolean Gender=Console.ReadLine() == "m" ? true : false;

            Date birth = new Date();
            Console.WriteLine("Enter year of birth");
            birth.Year = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Month of birth");
            birth.Month = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Day of birth");
            birth.Day = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Address");
            string Address = Console.ReadLine();

            Console.WriteLine("Enter salary");
            double Salary = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Select Employee \n Type 0:Trainee, 1:Permanent, 2:Temporary");
            EmployeeTypes EmpType = (EmployeeTypes)Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Commision");
            Double Commision = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Enter Email ID");
            string email_id = Console.ReadLine();

            Console.WriteLine("Enter Passcode");
            string passcode = Console.ReadLine();

            Console.WriteLine("Enter Department Number from bellow");
            viewDepartment();
            int dept_no = Convert.ToInt32(Console.ReadLine());

            Date hiredate = new Date();
            Console.WriteLine("Enter year of hire Date");
            hiredate.Year = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Month of hire Date");
            hiredate.Month = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Day of hire Date");
            hiredate.Day = Convert.ToInt32(Console.ReadLine());

            Salesperson sp = new Salesperson(Name,Address,birth,Gender,Salary,EmpType,Commision,email_id,passcode,dept_no,hiredate);
            salespersons.Add(sp);
        }
    }
}
