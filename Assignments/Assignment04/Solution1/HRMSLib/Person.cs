﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class Person
    {
        private string _name;
        private string _address;
        private Date _birth;
        private Boolean _gender;
        private string _email_Id;

        public string EmailId
        {
            get { return _email_Id; }
            set { _email_Id = value; }
        }


        public Person()
        {
            this.Name = "";
            this.Address = "";
            this.Birth = new Date();
            this.EmailId = "";
            this.setGender(true);
        }
        public Person(string name, string address,Date birth,Boolean gender, String emailID)
        {
            this.Name = name;
            this.Address = address;
            this.setGender(gender);
            this.Birth = birth;
            this.EmailId = emailID; 
        }
    
        protected void setGender(bool gender)
        {
            this._gender = gender;
        }
       
        public string getGender()
        {
            if (this._gender)
            {
                return "Male";
            }
            else
            {
                return "Female";
            }
        }



        public Date Birth
        {
            get { return _birth; }
            protected set { _birth = value; }
        }


        public string Address
        {
            get { return _address; }
            protected set { _address = value; }
        }

        public string Name
        {
            get { return _name; }
            protected set { _name = value; }
        }

        public override string ToString()
        {
            return "Name:" + this.Name.ToString() + ", Gender:" + this.getGender().ToString() + ", Address:" + this.Address.ToString() + ", Birth:" + this.Birth.ToString() + ", Age:" +this.GetAge().ToString() ;
        }
        private int GetAge()
        {
            int day = DateTime.Now.Day;
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            return this.Birth - new Date(year,month,day);
        }
    }
}
