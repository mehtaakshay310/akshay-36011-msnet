﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class Employee: Person
    {
        private static int count=1;
        private int _id;
        private double _salary;
        private double _hra;
        private double _da;
        private string _designation;
        private EmployeeTypes _empType;
        private string _passcode;
        private int _deptNo;
        private Date date;

        public Date HireDate
        {
            get { return date; }
            set { date = value; }
        }


        public int DeptNO
        {
            get { return _deptNo; }
            set { _deptNo = value; }
        }


        public string PassCode
        {
            get { return _passcode; }
            set { _passcode = value; }
        }


        public Employee() : base()
        {
            this.Id = count++;
            this.Salary = 0.0;
            this.Hra = 4 * this.Salary / 10;
            this.DA = this.Salary / 10;
            this.Designation = "";
            this.EmpType = 0;
            this.DeptNO = 0;
            this.HireDate = new Date();
            this.PassCode = "";
        }

        public Employee(string name, string address, Date birth, Boolean gender,double salary,string designation,EmployeeTypes empType, string email_id, string passcode,int deptno,Date hiredate) : base(name, address, birth, gender,email_id)
        {
            this.Id = count++;
            this.Salary = salary;
            this.Hra = 4 * this.Salary / 10;
            this.DA = this.Salary / 10;
            this.Designation = designation;
            this.EmpType = empType;
            this.DeptNO = deptno;
            this.HireDate = hiredate;
            this.PassCode = passcode;
        }
        public EmployeeTypes EmpType
        {
            get { return _empType; }
            set { _empType = value; }
        }

        public string Designation
        {
            get { return _designation; }
            set { _designation = value; }
        }

        public double DA
        {
            get { return _da; }
            protected set { _da = value; }
        }

        public double Hra
        {
            get { return _hra; }
            protected set { _hra = value; }
        }

        public double Salary
        {
            get { return _salary; }
            set { _salary = value; }
        }

        public int Id
        {
            get { return _id; }
            private set { _id = value; }
        }

        public override string ToString()
        {
            return base.ToString()+"\n Id:" + this.Id.ToString() + base.ToString() + " Salary:" + this.Salary + " Hra:" + this.Hra + " Da:" + this.DA + " Designation:" + this.Designation + " Employee Type:" + this.EmpType + " Passcode:" + this.PassCode + " DeptNo:" + this.DeptNO + " HireDate:" + this.HireDate.ToString();
        }

        public virtual double getNetSalary()
        {
            return this.Salary + this.Hra + this.DA;
        }
    }
}
