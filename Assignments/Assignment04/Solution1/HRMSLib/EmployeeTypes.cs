﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public enum EmployeeTypes
    {
        Trainee,
        Permanent,
        Temporary
    }
}
