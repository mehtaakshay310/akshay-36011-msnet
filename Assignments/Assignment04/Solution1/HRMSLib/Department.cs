﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class Department
    {
        private int _deptNo;
        private string _deptName;
        private string _location;

        public Department()
        {
            this.DeptNo = 0;
            this.DeptName = "";
            this.Location = "";
        }

        public Department(int deptNo, string deptName, string location)
        {
            this.DeptNo = deptNo;
            this.DeptName = deptName;
            this.Location = location;
        }

        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }


        public string DeptName
        {
            get { return _deptName; }
            set { _deptName = value; }
        }


        public int DeptNo
        {
            get { return _deptNo; }
            set { _deptNo = value; }
        }

        public override string ToString()
        {
            return "Department Number:" + this.DeptNo.ToString() + " Department Name:" + this.DeptName.ToString() + " Location:" + this.Location.ToString();
        }
    }
}
