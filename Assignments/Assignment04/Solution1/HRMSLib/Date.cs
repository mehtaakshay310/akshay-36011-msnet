﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class Date
    {
        private int _day;
        private int _Month;
        private int _Year;

        public Date()
        {
            this.Year = 1900;
            this.Month = 1;
            this.Day = 1;
        }

        public Date(int year,int month,int day)
        {
            this.Year = year;
            this.Month = month;
            this.Day = day;
        }

        public int Year
        {
            get {
                return this._Year; 
            }
            set 
            {
                if (1900 <= value && value < 2100)
                {
                    this._Year = value;
                }
                else
                {
                    throw new exception("Enter Year between 20th and 21st Century Only");
                }
            }
        }

        public int Month
        {
            get { return _Month; }
            set 
            {
                if (1 <= value && value <= 12)
                {
                    this._Month = value;
                }
                else
                {
                    throw new exception("Enter Appropriate Month");
                }
            }
        }

        public int Day
        {
            get { return this._day; }
            set 
            {
                if (value > 0)
                {
                    if(Month == 1 || Month == 3 || Month == 5 || Month == 7 || Month == 8 || Month == 10 || Month == 12)
                    {
                        if(value <= 31)
                        {
                            _day = value;
                        }
                        else
                        {
                            throw new exception("Your month is "+Month+" which has days between 1 to 31");
                        }
                    }
                    else if (Month == 2 )
                    {
                        if(((Year % 4 == 0) && (Year % 100 != 0)) || (Year % 400 == 0))
                        {
                            if(value <= 29)
                            {
                                _day = value;
                            }

                            else
                            {
                                throw new exception("Your month is " + Month + " which has days between 1 to 29");
                            }
                        }
                        else
                        {
                            if (value <= 28)
                            {
                                _day = value;
                            }

                            else
                            {
                                throw new exception("Your month is " + Month + " which has days between 1 to 28");
                            }
                        }
                    }
                    else
                    {
                        if (value <= 30)
                        {
                            _day = value;
                        }
                        else
                        {
                            throw new exception("Your month is " + Month + " which has days between 1 to 30");
                        }
                    }
                }
                else
                {
                    throw new exception("Day Cant be less than 1");
                }
            }
        }

        public override string ToString()
        {
            return Day + "/" + Month + "/" + Year;
        }

        public static int operator -(Date d1, Date d2)
        {
            if(d1.Month > d2.Month)
            {
                return Math.Abs(d1.Year - d2.Year);
            }
            else
            {
                return Math.Abs(d1.Year - d2.Year)-1;
            }
        }
    }
}
