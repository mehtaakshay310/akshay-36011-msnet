﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class WageEmp:Employee
    {
        private int _hours;
        private int _rate;

        public WageEmp() : base()
        {
            this.Hours = 0;
            this.Rate = 0;
        }

        public WageEmp(string name, string address, Date birth, Boolean gender, double salary, EmployeeTypes empType, int hours,int rate,string email_id, string passcode, int deptno, Date hiredate) : base(name, address, birth, gender, salary, "Wage", empType, email_id,passcode, deptno,hiredate)
        {
            this.Hours = hours;
            this.Rate = rate;
        }
        public int Rate
        {
            get { return _rate; }
            set { _rate = value; }
        }


        public int Hours
        {
            get { return _hours; }
            set { _hours = value; }
        }

        public override string ToString()
        {
            return base.ToString() + " Hours:" + this.Hours.ToString() +" Rate:"+ this.Rate.ToString();
        }

        public override double getNetSalary()
        {
            return this.Hours * this.Rate; 
        }
    }
}
