﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class Salesperson:Employee
    {
        private double _commision;

        public Salesperson() : base()
        {
            this.Commision = 0;
        }

        public Salesperson(string name, string address, Date birth, Boolean gender, double salary, EmployeeTypes empType, double commision,string email_id, string passcode, int deptno, Date hiredate) : base(name, address, birth, gender, salary, "Salesperson", empType,email_id,passcode,deptno,hiredate)
        {
            this.Commision = commision;
        }

        public double Commision
        {
            get { return _commision; }
            set { _commision = value; }
        }
        public override string ToString()
        {
            return base.ToString()+" Commision"+this.Commision;
        }

        public override double getNetSalary()
        {
            return base.getNetSalary() + this.Commision;
        }
    }
}
