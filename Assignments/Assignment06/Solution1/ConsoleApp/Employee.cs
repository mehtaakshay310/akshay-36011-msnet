﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    public class Employee
    {
        private int _empNo;
        private string _name;
        private string  _designation;
        private double _salary;
        private double _commision;
        private int _deptNo;

        public Employee()
        {
            EmpNo = 0;
            Name = "";
            Designation = "";
            Salary = 0.0;
            Commision = 0.0;
            DeptNo = 0;
        }

        public Employee(int empNo, string name, string designation, double salary, double commision, int deptNo)
        {
            EmpNo = empNo;
            Name = name;
            Designation = designation;
            Salary = salary;
            Commision = commision;
            DeptNo = deptNo;
        }

        public int DeptNo
        {
            get { return _deptNo; }
            set { _deptNo = value; }
        }


        public double Commision
        {
            get { return _commision; }
            set { _commision = value; }
        }


        public double Salary
        {
            get { return _salary; }
            set { _salary = value; }
        }


        public string  Designation
        {
            get { return _designation; }
            set { _designation = value; }
        }


        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        public int EmpNo
        {
            get { return _empNo; }
            set { _empNo = value; }
        }

        public override string ToString()
        {
            return "***Employee***"+
                "\n\tEmp No: " + this.EmpNo +
                "\n\tName: " + this.Name +
                "\n\tDesignation: " + this.Designation +
                "\n\tSalary: " + this.Salary +
                "\n\tCommision: " + this.Commision +
                "\n\tDept No: " + this.DeptNo+"\n";
        }

    }
}
