﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Department
    {
        private int _deptNo;
        private string _deptName;
        private string _location;

        public Department()
        {
            DeptNo = 0;
            DeptName = "";
            Location = "";
        }

        public Department(int deptNo, string deptName,string location)
        {
            DeptNo = deptNo;
            DeptName = deptName;
            Location = location;
        }

        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }


        public string DeptName
        {
            get { return _deptName; }
            set { _deptName = value; }
        }


        public int DeptNo
        {
            get { return _deptNo; }
            set { _deptNo = value; }
        }


        public override string ToString()
        {
            return "***Department***" +
                "\n\tDept No: " + this.DeptNo +
                "\n\tDepartment Name: " + this.DeptName+
                "\n\tLocation: " + this.Location+"\n";
        }
    }
}
