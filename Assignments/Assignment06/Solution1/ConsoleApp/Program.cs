﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static Dictionary<int,Department> DeptList = new Dictionary<int, Department>();
        static Dictionary<int,Employee> EmpList = new Dictionary<int, Employee>();
        static void Main(string[] args)
        {
            getDepartmentDetails();
            getEmployeeDetails();

            //List<string >locationList =LocationWithSingleDept();
            //foreach (string location in locationList)
            //{
            //    Console.WriteLine(location);
            //}

            //List<string >departmentNames = FindDeptsWithNoEmps();
            //foreach (string dname in departmentNames)
            //{
            //    Console.WriteLine(dname);
            //}

            //List<double> empTotalSalary=Calcluate_Total_Salary();
            //foreach (double salary in empTotalSalary)
            //{
            //    Console.WriteLine(salary);
            //}

            //Console.WriteLine("Enter Department Number:");
            //int deptNo = Convert.ToInt32(Console.ReadLine());
            //GetAllEmployeesByDept(deptNo);

            //Dictionary<int, int> employeeCount = DeptwiseStaffCount();
            //foreach (int key in employeeCount.Keys)
            //{
            //    Console.WriteLine(employeeCount[key]);
            //}

            //Dictionary<int, double> avgSalaryD = DeptwiseAvgSal();
            //foreach (int key in avgSalaryD.Keys)
            //{
            //    Console.WriteLine(avgSalaryD[key]);
            //}

            Dictionary<int, double> minSalaryD = DeptwiseMinSal();
            foreach (int key in minSalaryD.Keys)
            {
                Console.WriteLine(minSalaryD[key]);
            }

        }

        private static Dictionary<int, double> DeptwiseMinSal()
        {
            Dictionary<int, double> minSalaryD = new Dictionary<int, double>();
            foreach (int deptKey in DeptList.Keys)
            {
                double minSalary = Double.MaxValue;
                foreach (int empKey in EmpList.Keys)
                {
                    if (EmpList[empKey].DeptNo == deptKey && minSalary > EmpList[empKey].Salary)
                    {
                        minSalary = EmpList[empKey].Salary;
                    }
                }
                if(minSalary==Double.MaxValue)
                {
                    minSalary = 0;
                }
                minSalaryD.Add(deptKey,minSalary);
            }
            return minSalaryD;
        }

        private static Dictionary<int, double> DeptwiseAvgSal()
        {
            Dictionary<int, double> avgSalaryD = new Dictionary<int, double>();
            foreach (int deptKey in DeptList.Keys)
            {
                double Salary = 0;
                double count = 0;
                foreach (int empKey in EmpList.Keys)
                {
                    if (EmpList[empKey].DeptNo == deptKey)
                    {
                        count++;
                        Salary += EmpList[empKey].Salary;
                    }
                }
                avgSalaryD.Add(deptKey, Salary / count);
            }
            return avgSalaryD;
        }

        private static Dictionary<int, int> DeptwiseStaffCount()
        {
            Dictionary<int, int> employeeCount = new Dictionary<int, int>();
            foreach (int deptKey in DeptList.Keys)
            {
                int count = 0;
                foreach (int empKey in EmpList.Keys)
                {
                    if(EmpList[empKey].DeptNo==deptKey)
                    {
                        count++;
                    }
                }
                employeeCount.Add(deptKey, count);
            }
            return employeeCount;
        }

        private static void GetAllEmployeesByDept(int deptNo)
        {
            Console.WriteLine("List Of EMployees in department NO "+deptNo);
            foreach (int empKey in EmpList.Keys)
            {
                if(EmpList[empKey].DeptNo==deptNo)
                {
                    Console.WriteLine(EmpList[empKey]);
                }
            }
        }

        private static List<double> Calcluate_Total_Salary()
        {
            List<double> empTotalSalary = new List<double>();
            foreach (int  empkey in EmpList.Keys)
            {
                double totalSalary = EmpList[empkey].Salary + EmpList[empkey].Commision;
                empTotalSalary.Add(totalSalary);
            }
            return empTotalSalary;
        }

        private static List<string> FindDeptsWithNoEmps()
        {
            List<string> deptName = new List<string>();
            foreach (int deptkey in DeptList.Keys)
            {
                int count = 0;
                foreach (int key in EmpList.Keys)
                {
                    if (EmpList[key].DeptNo == deptkey)
                    {
                        count++;
                    }
                }
                if (count == 0)
                {
                    deptName.Add(DeptList[deptkey].DeptName);
                }
            }
            return deptName;
        }


        private static List<string> LocationWithSingleDept()
        {
            List<string> locationList = new List<string>();
            foreach (int key in DeptList.Keys)
            {
                int count = 0;
                foreach (int key1 in DeptList.Keys)                
                    if(DeptList[key].Location == DeptList[key1].Location)
                        count++;                    
                if(count==1)                
                    locationList.Add(DeptList[key].Location);                
            }
            return locationList;
        }

        #region Get Details from Csvs
        private static void getEmployeeDetails()
        {
            FileStream fs = new FileStream(@"D:\MsNet\workspace\akshay-36011-msnet\Assignments\Assignment06\emp.csv", FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(fs);
            string empstring;
            while ((empstring = reader.ReadLine()) != null)
            {
                string[] empDetails = empstring.Split(',');
                Employee emp = new Employee();
                emp.EmpNo = int.Parse(empDetails[0]);
                emp.Name = empDetails[1];
                emp.Designation = empDetails[2];
                emp.Salary = double.Parse(empDetails[3]);
                emp.Commision = double.Parse(empDetails[4]);
                emp.DeptNo = int.Parse(empDetails[5]);
                EmpList.Add(emp.EmpNo, emp);
            }
            reader.Close();
            fs.Close();
        }

        private static void getDepartmentDetails()
        {
            FileStream fs = new FileStream(@"D:\MsNet\workspace\akshay-36011-msnet\Assignments\Assignment06\dept.csv", FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(fs);
            string deptstring;
            while ((deptstring = reader.ReadLine()) != null)
            {
                string[] deptDetails = deptstring.Split(',');
                Department dept = new Department();
                dept.DeptNo = int.Parse(deptDetails[0]);
                dept.DeptName = deptDetails[1];
                dept.Location = deptDetails[2];

                DeptList.Add(dept.DeptNo, dept);
            }
            reader.Close();
            fs.Close();
        } 
        #endregion
    }
}