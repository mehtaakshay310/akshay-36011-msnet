﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyLoggerLib;

namespace Database
{
    class program
    {
        static void Main(string[] args)
        {
            try
            {
                MyLogger.CurrentLogger.Log("Main method Has been Called..");
                Console.WriteLine("Select Your Database");
                Console.WriteLine("1: Sql Server, 2: Oracle");
                int dbChoice = Convert.ToInt32(Console.ReadLine());
                databaseFactory df = new databaseFactory();
                Database d = df.GetDatabase(dbChoice);
                Console.WriteLine("Select Operation to Perform");
                Console.WriteLine("1. Insert , 2: Update, 3:Delete");
                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        MyLogger.CurrentLogger.Log("Insert Method inside Database has been Called");
                        d.Insert();
                        break;
                    case 2:
                        MyLogger.CurrentLogger.Log("Update Method inside Database has been Called");
                        d.Update();
                        break;
                    case 3:
                        MyLogger.CurrentLogger.Log("Delete Method inside Database has been Called");
                        d.Delete();
                        break;
                    default:
                        break;
                }
                Console.ReadLine();

            }
            catch (Exception ex)
            {
                MyLogger.CurrentLogger.Log(ex.Message);
            }
            MyLogger.CurrentLogger.Log("Console Application Closed");
        }
    }

    public class databaseFactory
    {
        public Database GetDatabase(int choice)
        {
            if (choice == 1)
            {
                MyLogger.CurrentLogger.Log("Sql Database Has been Selected");
                return new Sql();
            }
            else
            {
                MyLogger.CurrentLogger.Log("Oracle Database Has been Selected");
                return new Oracle();
            }
        }
    }

    public interface Database
    {
        void Insert();
        void Update();
        void Delete();
    }

    public class Oracle : Database
    {
        public void Delete()
        {
            Console.WriteLine("Deleted Into Oracle");
        }

        public void Insert()
        {
            Console.WriteLine("Inserted Into Oracle");
        }

        public void Update()
        {
            Console.WriteLine("Updated Into Oracle");
        }
    }
    public class Sql : Database
    {
        public void Delete()
        {
            Console.WriteLine("Deleted Into Sql");
        }

        public void Insert()
        {
            Console.WriteLine("Inserted Into Sql");
        }

        public void Update()
        {
            Console.WriteLine("Updated Into Sql");
        }
    }

}
