﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLoggerLib
{
    public class MyLogger: ILogger
    {
        private static MyLogger _logger = new MyLogger();

        private MyLogger()
        {
            Console.WriteLine("Logger object Created");
        }

        public static MyLogger CurrentLogger
        {
            get
            {
                return _logger;
            }
        }

        public void Log(string message)
        {
            FileStream fs = new FileStream(@"D:\MsNet\workspace\akshay-36011-msnet\Assignments\Assignmnet05\Log\log" + DateTime.Now.ToString("ddMMyyyy") + ".csv", FileMode.Append, FileAccess.Write);
            StreamWriter writer = new StreamWriter(fs);

            string msg = DateTime.Now.ToString() + ", " + message;
            Console.WriteLine(msg);
            writer.WriteLine(msg);

            writer.Close();
            fs.Close();
        }
    }
}
