﻿using BasicNS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TempratureNS;

namespace MyCalci
{
    class Program
    {
        static void Main(string[] args)
        {            
            string isMoreCalculate = "y";
            do
            {
            Console.WriteLine("Select which calculation you want to perform");

            Console.WriteLine("1. Basic Calculation, 2. Temprature Calculation");
            int choice=Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        basicCalculation();
                        break;
                    case 2:
                        tempratureCalculation();
                        break;
                    default:
                        Console.WriteLine("Enter Appropriate choice");
                        break;
                }

                Console.WriteLine("Do You Want to Perform More calculation? (y/n)");
                isMoreCalculate = Console.ReadLine();

            } while (isMoreCalculate == "y");   
        }

        static void basicCalculation()
        {
            BasicCalculator basicCalculator = new BasicCalculator();

            Console.WriteLine("Select which Operation you want to perform");
            Console.WriteLine("1: Add, 2: Sub, 3: Mul, 4: Div");
            int choice = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter value of first number");
            int firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter value of second number");
            int secondNumber = Convert.ToInt32(Console.ReadLine());

            float result = 0;

            switch (choice)
            {
                case 1:
                    result = basicCalculator.Add(firstNumber, secondNumber);
                    break;
                case 2:
                    result = basicCalculator.Sub(firstNumber, secondNumber);
                    break;
                case 3:
                    result = basicCalculator.Mul(firstNumber, secondNumber);
                    break;
                case 4:
                    result = basicCalculator.Div(firstNumber, secondNumber);
                    break;
                default:
                    Console.WriteLine("Enter Appropriate choice");
                    break;
            }
            Console.Write("Result=" + result);
        }

        static void tempratureCalculation()
        {
            TempratureConverter tempratureConverter = new TempratureConverter();
            Console.WriteLine("Select which Operation you want to perform");
            Console.WriteLine("1: Farenheit To Celcius, 2: CelciusToFarenheit");
            int choice = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter temprature");
            int tempature = Convert.ToInt32(Console.ReadLine());

            float result = 0;

            switch (choice)
            {
                case 1:
                    result = tempratureConverter.FarenheitToCelcius(tempature);
                    break;
                case 2:
                    result = tempratureConverter.CelciusToFarenheit(tempature);
                    break;
                default:
                    Console.WriteLine("Enter Appropriate choice");
                    break;
            }
            Console.Write("Result=" + result);
        }
    }
}
