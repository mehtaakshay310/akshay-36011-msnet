﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicNS
{
    public class BasicCalculator
    {
        public int Add(int x,int y)
        {
            return x + y;
        }

        public int Sub(int x, int y)
        {
            return x - y;
        }

        public int Div(int x, int y)
        {
            return x / y;
        }

        public int Mul(int x, int y)
        {
            return x * y;
        }
    }
}

namespace TempratureNS
{
    public class TempratureConverter
    {
        public float FarenheitToCelcius(float farenheit)
        {
            float celcius = (farenheit - 32) * (5 / 9);
            return celcius;
        }

        public float CelciusToFarenheit(float Celcius)
        {
            float farenheit = Celcius * (9 / 5) + 32;
            return farenheit;
        }
    }
}