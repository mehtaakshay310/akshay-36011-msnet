﻿using Crud_modular_way.POCO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crud_modular_way.DAL
{
    public class SQLServer : IDatabase
    {
        public int Delete(int No)
        {
            string constr = ConfigurationManager.ConnectionStrings["conString"].ToString();
            SqlConnection con = new SqlConnection(constr);
            string DeleteTemplate = "Delete from Emp where Number=@No";
            string query = string.Format(DeleteTemplate, No);
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandType = CommandType.Text;
            SqlParameter parameter1 = new SqlParameter("@No", SqlDbType.Int);
            parameter1.Value = No;

            cmd.Parameters.Add(parameter1);

            con.Open();
            int rowsAffected = cmd.ExecuteNonQuery();
            con.Close();
            return rowsAffected;
        }

        public int Insert(Emp emp)
        {
            string constr = ConfigurationManager.ConnectionStrings["conString"].ToString();
            SqlConnection con = new SqlConnection(constr);
            
            SqlCommand cmd = new SqlCommand("spInsert", con);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter1 = new SqlParameter("@No", SqlDbType.Int);
            parameter1.Value =Convert.ToInt32(emp.Number);

            SqlParameter parameter2 = new SqlParameter("@Name", SqlDbType.VarChar,50);
            parameter2.Value = emp.Name;

            SqlParameter parameter3 = new SqlParameter("@Address", SqlDbType.VarChar,50);
            parameter3.Value = emp.Address;

            cmd.Parameters.Add(parameter1);
            cmd.Parameters.Add(parameter2);
            cmd.Parameters.Add(parameter3);

            con.Open();
            int rowsAffected = cmd.ExecuteNonQuery();
            con.Close();
            return rowsAffected;
        }

        public List<Emp> select()
        {
            List<Emp> empList = new List<Emp>();

            string constr = ConfigurationManager.ConnectionStrings["conString"].ToString();
            SqlConnection con = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand("Select * from Emp", con);

            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            while(reader.Read())
            {
                Emp emp = new Emp() { Number = Convert.ToInt32(reader["Number"]),Address=reader["Address"].ToString(),Name=reader["Name"].ToString() };
                empList.Add(emp);
            }
            con.Close();
            return empList;
        }

        public int Update(Emp emp)
        {
            string constr = ConfigurationManager.ConnectionStrings["conString"].ToString();
            SqlConnection con = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand("spUpdate", con);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter parameter1 = new SqlParameter("@No", SqlDbType.Int);
            parameter1.Value = Convert.ToInt32(emp.Number);

            SqlParameter parameter2 = new SqlParameter("@Name", SqlDbType.VarChar, 50);
            parameter2.Value = emp.Name;

            SqlParameter parameter3 = new SqlParameter("@Address", SqlDbType.VarChar, 50);
            parameter3.Value = emp.Address;

            cmd.Parameters.Add(parameter1);
            cmd.Parameters.Add(parameter2);
            cmd.Parameters.Add(parameter3);


            con.Open();
            int rowsAffected = cmd.ExecuteNonQuery();
            con.Close();
            return rowsAffected;
        }
    }
}
