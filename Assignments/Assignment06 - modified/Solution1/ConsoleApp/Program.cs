﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static Dictionary<int,Department> DeptList = new Dictionary<int, Department>();
        static Dictionary<int,Employee> EmpList = new Dictionary<int, Employee>();
        static void Main(string[] args)
        {
            getDepartmentDetails();
            getEmployeeDetails();

            //List<string> locationList = LocationWithSingleDept();
            //foreach (string location in locationList)
            //{
            //    Console.WriteLine(location);
            //}

            //List<string> departmentNames = FindDeptsWithNoEmps();
            //FindDeptsWithNoEmps();
            //foreach (string dname in departmentNames)
            //{
            //    Console.WriteLine(dname);
            //}

            //List<double> empTotalSalary = Calcluate_Total_Salary();
            //foreach (double salary in empTotalSalary)
            //{
            //    Console.WriteLine(salary);
            //}

            //Console.WriteLine("Enter Department Number:");
            //int deptNo = Convert.ToInt32(Console.ReadLine());
            //GetAllEmployeesByDept(deptNo);

            //Dictionary<int, int> employeeCount = DeptwiseStaffCount();
            //foreach (int key in employeeCount.Keys)
            //{
            //    Console.WriteLine(employeeCount[key]);
            //}

            //Dictionary<int, double> avgSalaryD = DeptwiseAvgSal();
            //foreach (int key in avgSalaryD.Keys)
            //{
            //    Console.WriteLine(avgSalaryD[key]);
            //}


            //Dictionary<int, double> minSalaryD = DeptwiseMinSal();
            //foreach (int key in minSalaryD.Keys)
            //{
            //    Console.WriteLine(minSalaryD[key]);
            //}
            GetEmpInfo();
            
        }

        private static void GetEmpInfo()
        {
            var empInfo = (from emp in EmpList
                           select new { Eno = emp.Value.EmpNo, Ename = emp.Value.Name, Dname = DeptList[emp.Value.DeptNo].DeptName }
                           ).ToDictionary(x=>x.Eno,x=>new { Ename = x.Ename, deptname= x.Dname });
            foreach (var item in empInfo)
            {
                Console.WriteLine("Employee No: "+item.Eno+ "\tEmployee Name: "+item.Eno + "\t Department Name: " + item.Dname);
            }
        }

        private static Dictionary<int, double> DeptwiseMinSal()
        {
            Dictionary<int, double> minSalaryD = (from dept in DeptList
                                                  join emp in EmpList on dept.Value.DeptNo equals emp.Value.DeptNo into grp
                                                  select new { DeptId = dept.Value.DeptNo, Min = (grp.Count() > 0) ? grp.Min(x => x.Value.Salary) : 0 }
                            ).ToDictionary(g => g.DeptId, g => g.Min);
            return minSalaryD;
        }

        private static Dictionary<int, double> DeptwiseAvgSal()
        {

            Dictionary<int, double> avgSalaryD = (from dept in DeptList
                             join emp in EmpList on dept.Value.DeptNo equals emp.Value.DeptNo into grp
                             select new { DeptId = dept.Value.DeptNo, Avg = (grp.Count() > 0) ? grp.Average(x => x.Value.Salary) : 0 }
                            ).ToDictionary(g=>g.DeptId,g=>g.Avg);
            return avgSalaryD;
        }

        private static Dictionary<int, int> DeptwiseStaffCount()
        {
            Dictionary<int, int> employeeCount = (from dept in DeptList
                                    join emp in EmpList on dept.Value.DeptNo equals emp.Value.DeptNo into joined
                                    select new { deptno=dept.Value.DeptNo, count=joined.Count() }
                                    ).ToDictionary(g=> g.deptno, g=> g.count);
            return employeeCount;
        }

        private static void GetAllEmployeesByDept(int deptNo)
        {
            Console.WriteLine("List Of EMployees in department NO " + deptNo);

            var empListOfDepartment = (from emp in EmpList
                                       where emp.Value.DeptNo == deptNo
                                       select emp.Value
                                       ).ToList();

            foreach (var emp in empListOfDepartment)
            {
                Console.WriteLine(emp);
            }
        }
        private static List<double> Calcluate_Total_Salary()
        {
            List<double> empTotalSalary = (from emp in EmpList
                                           select emp.Value.Commision+emp.Value.Salary
                                           ).ToList();
            return empTotalSalary;
        }

        private static List<string> FindDeptsWithNoEmps()
        {
            var deptName = (from dept in DeptList
                            join emp in EmpList on dept.Value.DeptNo equals emp.Value.DeptNo into joined
                            where joined.Count()==0
                            select dept.Value.DeptName).ToList();

            return deptName;
        }

        private static List<string> LocationWithSingleDept()
        {
            var locationList = (from dpt in DeptList
                                group dpt by dpt.Value.Location into location
                                where location.Count()== 1
                                select location.Key).ToList();
            return locationList;
        }

        #region Get Details from Csvs
        private static void getEmployeeDetails()
        {
            FileStream fs = new FileStream(@"D:\MsNet\workspace\akshay-36011-msnet\Assignments\Assignment06\emp.csv", FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(fs);
            string empstring;
            while ((empstring = reader.ReadLine()) != null)
            {
                string[] empDetails = empstring.Split(',');
                Employee emp = new Employee();
                emp.EmpNo = int.Parse(empDetails[0]);
                emp.Name = empDetails[1];
                emp.Designation = empDetails[2];
                emp.Salary = double.Parse(empDetails[3]);
                emp.Commision = double.Parse(empDetails[4]);
                emp.DeptNo = int.Parse(empDetails[5]);
                EmpList.Add(emp.EmpNo, emp);
            }
            reader.Close();
            fs.Close();
        }

        private static void getDepartmentDetails()
        {
            FileStream fs = new FileStream(@"D:\MsNet\workspace\akshay-36011-msnet\Assignments\Assignment06\dept.csv", FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(fs);
            string deptstring;
            while ((deptstring = reader.ReadLine()) != null)
            {
                string[] deptDetails = deptstring.Split(',');
                Department dept = new Department();
                dept.DeptNo = int.Parse(deptDetails[0]);
                dept.DeptName = deptDetails[1];
                dept.Location = deptDetails[2];

                DeptList.Add(dept.DeptNo, dept);
            }
            reader.Close();
            fs.Close();
        } 
        #endregion
    }
}