﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLoggerLib
{
    class Logger
    {
        private static Logger _logger = new Logger();

        private Logger()
        {
            Console.WriteLine("Logger object Created");
        }

        public static Logger CurrentLogger
        {
            get
            {
                return _logger;
            }
        }

        public void Log(string msg)
        {
            Console.WriteLine("Logged: " + msg + " @ " + DateTime.Now.ToString());
        }
    }
}
