﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace book
{
    class Program
    {
        static void Main(string[] args)
        {
            Book b1 = new Book("AKshay",false,"akshay",123,'a',500);
            b1.PrintDetails();
            Book b2 = new Book();
            b2.AcceptDetails();
            b2.PrintDetails();
            Console.ReadLine();
        }
    }

    struct Book
    {
        public string title; 
        private bool outofstock; 
        private string author;
        private int isbn; 
        private char index; 
        private double price;

        public Book(string title, bool outofstock,string author, int isbn, char index, double  price)
        {
            this.title = title;
            this.outofstock = outofstock;
            this.author = author;
            this.isbn = isbn;
            this.index = index;
            this.price = price;
        }
        
        public void setTitle(String title)
        {
            this.title = title;
        }

        public void setOutOfStock(Boolean outofstock)
        {
            this.outofstock = outofstock;
        }

        public void setAuthor(String author)
        {
            this.author = author;
        }

        public void setIsbn(int isbn)
        {
            this.isbn = isbn;
        }
        public void setIndex(char index)
        {
            this.index = index;
        }
        public void setPrice(Double price)
        {
            this.price = price;
        }

        public string getTitle()
        {
            return this.title;
        }

        public Boolean getOutOfStock()
        {
            return this.outofstock;
        }

        public String getAuthor()
        {
            return this.author;
        }

        public int getIsbn()
        {
            return this.isbn;
        }

        public char getIndex()
        {
            return this.index;
        }

        public Double getPrice()
        {
            return this.price;
        }

        public void AcceptDetails()
        {
            Console.WriteLine("Enter Title");
            this.title = Console.ReadLine();
            Console.WriteLine("Enter OutOfStock");
            this.outofstock = Convert.ToBoolean(Console.ReadLine());
            Console.WriteLine("Enter author");
            this.author = Console.ReadLine();
            Console.WriteLine("Enter Isbn");
            this.isbn = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter index");
            this.index = Convert.ToChar(Console.ReadLine());
            Console.WriteLine("Enter price");
            this.price = Convert.ToDouble(Console.ReadLine());
        }

        public void PrintDetails()
        {
            Console.WriteLine("Title: "+title);
            Console.WriteLine("Out of Stock: "+outofstock);
            Console.WriteLine("Author: "+author);
            Console.WriteLine("Isbn: "+isbn);
            Console.WriteLine("Index: "+index);
            Console.WriteLine("Price: "+price);
        }
    }
}
