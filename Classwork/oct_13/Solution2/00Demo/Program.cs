﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Partial Class Usage

            Maths maths = new Maths();

            Console.WriteLine(maths.Add(10, 20));
            Console.WriteLine(maths.sub(20, 10)); 
            #endregion
        }
    }
}
