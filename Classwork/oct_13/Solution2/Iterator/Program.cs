﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator
{
    class Program
    {
        static void Main(string[] args)
        {
            Week week = new Week();
            foreach (string day in week)
            {
                Console.WriteLine(day);
            }
        }
    }

    public class Week : IEnumerable
    {
        private string[] days =
                   new string[] { "Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun" };
        public IEnumerator GetEnumerator()
        {
            for (int i = 0; i < days.Length; i++)
            {
                yield return days[i];
            }
        }
    }
}
