﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nullable_Type
{
    class Program
    {
        static void Main(string[] args)
        {
            Nullable<int> Salary = 10;
            if(Salary.HasValue)
            {
                Console.WriteLine("Salary holds a value");
            }
            else
            {
                Console.WriteLine("Salary is NULL");
            }
        }
    }
}
