﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implicit_Type
{
    class Program
    {
        static void Main(string[] args)
        {
            int choice = Convert.ToInt32(Console.ReadLine());
            var v = GetSomething(choice);

            var v1 = 100;

            var v2 = new Emp();

        }
        public static object GetSomething(int i)
        {
            if (i == 1)
            {
                return 100;
            }
            else
            {
                return new object();
            }

        }
    }
    public class Emp
    {
        private string _Name;
        private bool _IsConsultant;
        private string _Address;

        public string Addrress
        {
            get { return _Address; }
            set { _Address = value; }
        }

        public bool IsConsultant
        {
            get { return _IsConsultant; }
            set { _IsConsultant = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


    }
}
