﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_Type
{
    class Program
    {
        static void Main(string[] args)
        {
            var obj1 = new { No = 100, Name = "Akshay", Age = 22 };
            var obj2 = new { No = 100, Name = 22, Age = "22" };
            var obj3 = new { No = 100, Age = "Akshay", Name = 22 };
            Console.WriteLine(obj1.Name);
        }
    }
}
