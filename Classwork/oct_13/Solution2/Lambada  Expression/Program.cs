﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lambada__Expression
{
    class Program
    {
        delegate string myDelegate(string name);
        static void Main(string[] args)
        {
            myDelegate pointer = (name) =>
            {
                return "Hello " + name;
            };

            string message = pointer("akshay");
            Console.WriteLine(message);
        }
    }
}
