﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Object_Initializer
{
    class Program
    {
        static void Main(string[] args)
        {
            var emp = new Emp() { Address = "thane", Name = "Akshay", No = 100, Age = 40 };
            Console.WriteLine(emp.Name);
        }
    }


    public class Emp
    {
        private int _Age;

        public int Age
        {
            get { return _Age; }
            set { _Age = value; }
        }


        public int No { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
