﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auto_Property
{
    class Program
    {
        static void Main(string[] args)
        {
            Emp emp = new Emp();
            emp.No = 100;
            emp.Name = "akshay";
            emp.Address = "Thane";
        }
    }

    class Emp
    {
        public string Address { get; set; }
        public string Name { get; set; }
        public int No { get; set; }
    }
}
