﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonnymous_Method
{
    delegate string MyDelegate(string name);
    class Program
    {
        static void Main(string[] args)
        {
            ////Normal Call Method
            //string message = sayhello("AKshay");
            //Console.WriteLine(message);


            ////Using delegate method call
            //MyDelegate pointer = new MyDelegate(sayhello);
            //Console.WriteLine(pointer("Akshay"));

            //Call method which is anonymous
            MyDelegate myDelegate = delegate (string name)
                                             {
                                                 return "Hello " + name;
                                             };
            string message = myDelegate("akshay");
            Console.WriteLine(message);

        }

        private static string sayhello(string name)
        {
            return "Hello "+name;
        }
    }
}
