﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempAssignment
{
    public delegate string myDelegate();
    class Program
    {
        static void Main(string[] args)
        {
            B objB = new B();
            A objA = new A();
            myDelegate pointer = new myDelegate(objB.M2);
            objA.M1(pointer);

        }
    }

    public class A
    {
        //You can pass one parameter to M1
        //but parameter shoud not be of type string
        //parameter shoud not be of type  B 
        //parameter shoud not be of type Object
        public void M1(myDelegate pointer)
        {
            // Here you will have to call M2 method from B class
            //and print here what M2 returns on the screen..

            Console.WriteLine(pointer());
            // but conditions are: Here in this code of M1
            // 1. You wil not declare B Object
            // 2. No usage of Generics / Inheritance / Overloading  OverRiding 
            // 3. No Usage of Static / abstract / singletone
            // 4. No Usage of Collections
            // 5. No Events
            // 6. No File Io / Serialization
            // 7. No Partial Class, Nullbale Type, Anonymous Method, Lambada Expression to be used 
            // 8. No DLL Concept

        }
    }

    public class B
    {
        public string M2()
        {
            return "M2 from  B";
        }
    }
}
