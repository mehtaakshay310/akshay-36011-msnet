﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Consumer_1.Proxy;

namespace Consumer_1
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculatorSoapClient obj = new CalculatorSoapClient();
            Console.WriteLine(obj.Add(10,20));

            Console.ReadLine();
        }
    }
}
