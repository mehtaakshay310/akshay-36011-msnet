﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Consumer2.Proxy;

namespace Consumer2
{
    class Program
    {
        static void Main(string[] args)
        {
            WebService1SoapClient obj = new WebService1SoapClient();
            Console.WriteLine(obj.Add(10, 20));
            Console.WriteLine(obj.Sub(20,10));
        }
    }
}
