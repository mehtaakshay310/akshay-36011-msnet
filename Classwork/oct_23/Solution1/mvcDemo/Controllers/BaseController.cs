﻿using mvcDemo.Filter;
using mvcDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvcDemo.Controllers
{
    [HandleError(ExceptionType =typeof(Exception),View ="Error")]
    [customFilter]
    [Authorize]
    public class BaseController : Controller
    {
        protected SunbeamDBEntities dbObject { get; set; }
        public BaseController()
        {
            this.dbObject = new SunbeamDBEntities();
        }
    }
}