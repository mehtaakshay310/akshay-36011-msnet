﻿using mvcDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvcDemo.Controllers
{
    public class SampleController : BaseController
    {
        // GET: Sample
        public ActionResult Index()
        {
            ViewBag.Username = User.Identity.Name;
            ViewData["message"] = "Welcome to my app";
            List<Emp> allemps = dbObject.Emps.ToList();
            return View(allemps);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            ViewBag.Username = User.Identity.Name;

            Emp empToBeEdited = (from emp in dbObject.Emps.ToList()
                                 where emp.Number == id
                                 select emp
                       ).First();
            ViewBag.message = "Employee to Be added " + empToBeEdited.Name;
            return View(empToBeEdited);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Emp empUpdate)
        {
            Emp empToBeUpdate = (from emp in dbObject.Emps.ToList()
                                 where emp.Number == empUpdate.Number
                                 select emp
                       ).First();
            empToBeUpdate.Name = empUpdate.Name;
            empToBeUpdate.Address = empUpdate.Address;
            dbObject.SaveChanges();
            return Redirect("/Sample/Index");
        }

        public ActionResult Delete(int id)
        {
            Emp empToBeDeleted = (from emp in dbObject.Emps.ToList()
                                  where emp.Number == id
                                  select emp
                       ).First();
            dbObject.Emps.Remove(empToBeDeleted);
            dbObject.SaveChanges();
            return Redirect("/Sample/Index");
        }

        public ActionResult Create()
        {
            ViewBag.Username = User.Identity.Name;

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Emp empAdded)
        {
            if(ModelState.IsValid)
            {

            dbObject.Emps.Add(empAdded);
            dbObject.SaveChanges();
            return Redirect("/Sample/Index");
            }
            return View();
        }

        public ActionResult Validate(int id)
        {
            var count = (from emp in dbObject.Emps.ToList()
                         where emp.Number == id
                         select emp).Count();
            return new JsonResult() { Data = count, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}