﻿using Consumer3.proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consumer3
{
    class Program
    {
        static void Main(string[] args)
        {
            DbServiceClient proxydbobject = new DbServiceClient();
            foreach (var item in proxydbobject.GetEmps())
            {
                Console.WriteLine(item.Name);
            }
            

        }
    }
}
