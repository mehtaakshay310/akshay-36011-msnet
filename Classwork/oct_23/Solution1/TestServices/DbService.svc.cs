﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TestServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "DbService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select DbService.svc or DbService.svc.cs at the Solution Explorer and start debugging.
    public class DbService : IDbService
    {
        public List<Emp> GetEmps()
        {
            SunbeamDBEntities dbObj = new SunbeamDBEntities();
            return dbObj.Emps.ToList();
        }

        public string SayHi(string msg)
        {
            return ("Hi" + msg);
        }
    }
}
