﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Binary_Serialization__list
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Binary Serializtion of List
            //Emp emp = new Emp();

            //Console.WriteLine("Enter No:");
            //emp.No = Convert.ToInt32(Console.ReadLine());

            //Console.WriteLine("Enter Name");
            //emp.Name = Console.ReadLine();

            //Book b = new Book();

            //Console.WriteLine("Enter ISBN:");
            //b.ISBN = Convert.ToInt32(Console.ReadLine());

            //Console.WriteLine("Enter Title");
            //b.Title = Console.ReadLine();

            //List<object> list = new List<object>();
            //list.Add(emp);
            //list.Add(b);

            //FileStream fs = new FileStream(@"D:\MsNet\workspace\akshay-36011-msnet\Classwork\oct_09\Data.txt", FileMode.OpenOrCreate, FileAccess.Write);
            //BinaryFormatter binaryFormatter = new BinaryFormatter();

            //binaryFormatter.Serialize(fs, list);

            //binaryFormatter = null;

            //fs.Flush();
            //fs.Close(); 
            #endregion


            #region Binary De-Serialization of list
            //FileStream fs = new FileStream(@"D:\MsNet\workspace\akshay-36011-msnet\Classwork\oct_09\Data.txt", FileMode.Open, FileAccess.Read);

            //BinaryFormatter binaryFormatter = new BinaryFormatter();

            //object obj = binaryFormatter.Deserialize(fs);
            //if (obj is List<object>)
            //{
            //    List<object> arr = (List<object>)obj;
            //    foreach (object o in arr)
            //    {
            //        if (o is Emp)
            //        {
            //            Emp e = (Emp)o;
            //            Console.WriteLine(e.getDetails());
            //        }
            //        else if (o is Book)
            //        {
            //            Book b = (Book)o;
            //            Console.WriteLine("Details of the book : " + b.Title);
            //        }
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("Unknown type of data!!");
            //}

            //binaryFormatter = null;
            //fs.Flush();

            //fs.Close(); 
            #endregion
        }
    }


    [Serializable]
    public class Emp
    {
        [NonSerialized]
        private string _Password = "mahesh@123";
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return No.ToString() + " - " + Name;
        }

    }

    [Serializable]
    public class Book
    {
        private string _Title;
        private int _ISBN;

        public int ISBN
        {
            get { return _ISBN; }
            set { _ISBN = value; }
        }

        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        public string getDetails()
        {
            return ISBN.ToString() + " - " + Title;
        }

    }
}
