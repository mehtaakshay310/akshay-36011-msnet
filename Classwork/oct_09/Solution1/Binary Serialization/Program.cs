﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Binary_Serialization
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Binary Serialization
            //Employee e = new Employee();
            //Console.WriteLine("Enter Number");
            //e.No = Convert.ToInt32(Console.ReadLine());

            //Console.WriteLine("Enter Name");
            //e.Name = Console.ReadLine();

            //FileStream fs = new FileStream(@"D:\MsNet\workspace\akshay-36011-msnet\Classwork\oct_09\Data.txt", FileMode.OpenOrCreate, FileAccess.Write);
            //BinaryFormatter binaryFormatter = new BinaryFormatter();
            //binaryFormatter.Serialize(fs, e);

            //binaryFormatter = null;
            //fs.Flush();

            //fs.Close(); 
            #endregion

            #region Binary Deserialization
            //FileStream fs = new FileStream(@"D:\MsNet\workspace\akshay-36011-msnet\Classwork\oct_09\Data.txt", FileMode.Open, FileAccess.Read);

            //BinaryFormatter binaryFormatter = new BinaryFormatter();
            //object obj = binaryFormatter.Deserialize(fs);

            //if(obj is Employee)
            //{
            //    Employee e = (Employee)obj;
            //    Console.WriteLine(e.getDetails());
            //}
            //else
            //{
            //    Console.WriteLine("Unknown type of data!!");
            //}

            //binaryFormatter = null;
            //fs.Flush();

            //fs.Close(); 
            #endregion
        }
    }

    [Serializable]
    class Employee
    {
        [NonSerialized]
        private string _Password = "akhshay";
        private int  _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int  No
        {
            get { return _No; }
            set { _No = value; }
        }


        public string Password
        {
            get { return _Password ; }
            set { _Password = value; }
        }

        public string getDetails()
        {
            return No.ToString() + " - " + Name;
        }
    }
}
