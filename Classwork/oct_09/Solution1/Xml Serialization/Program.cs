﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Xml_Serialization
{
    class Program
    {
        static void Main(string[] args)
        {

            #region XML Serialization
            //Emp emp = new Emp();

            //Console.WriteLine("Enter No:");
            //emp.No = Convert.ToInt32(Console.ReadLine());

            //Console.WriteLine("Enter Name");
            //emp.Name = Console.ReadLine();

            //FileStream fs = new FileStream(@"D:\MsNet\workspace\akshay-36011-msnet\Classwork\oct_09\XMLData.txt", FileMode.OpenOrCreate, FileAccess.Write);
            //XmlSerializer xmlSerializer = new XmlSerializer(typeof(Emp));
            //xmlSerializer.Serialize(fs, emp);

            //xmlSerializer = null;
            //fs.Flush();

            //fs.Close(); 
            #endregion

            #region Xml Deserialization
            FileStream fs = new FileStream(@"D:\MsNet\workspace\akshay-36011-msnet\Classwork\oct_09\XMLData.txt",
                                               FileMode.Open,
                                               FileAccess.Read);

            XmlSerializer specialReader = new XmlSerializer(typeof(Emp));

            object obj = specialReader.Deserialize(fs);
            if (obj is Emp)
            {
                Emp e = (Emp)obj;
                Console.WriteLine(e.getDetails());
            }
            else
            {
                Console.WriteLine("Unknown type of data!!");
            }

            specialReader = null;
            fs.Flush();

            fs.Close(); 
            #endregion
        }
    }


    [Serializable]
    public class Emp
    {
        [NonSerialized]
        private string _Password = "mahesh@123";
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return No.ToString() + " - " + Name;
        }

    }
}
