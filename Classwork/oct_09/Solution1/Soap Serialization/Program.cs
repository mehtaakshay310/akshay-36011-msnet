﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using System.Threading.Tasks;

namespace Soap_Serialization
{
    class Program
    {
        static void Main(string[] args)
        {

            #region SOAP Serialization

            //Emp emp = new Emp();

            //Console.WriteLine("Enter No:");
            //emp.No = Convert.ToInt32(Console.ReadLine());

            //Console.WriteLine("Enter Name");
            //emp.Name = Console.ReadLine();

            //FileStream fs = new FileStream(@"D:\MsNet\workspace\akshay-36011-msnet\Classwork\oct_09\SoapData.xml",
            //                                FileMode.OpenOrCreate,
            //                                FileAccess.Write);

            //SoapFormatter specialwriter = new SoapFormatter();

            //specialwriter.Serialize(fs, emp);

            //specialwriter = null;
            //fs.Flush();

            //fs.Close();

            #endregion


            #region SOAP De-Serialization

            FileStream fs = new FileStream(@"D:\MsNet\workspace\akshay-36011-msnet\Classwork\oct_09\SoapData.xml",
                                            FileMode.Open,
                                            FileAccess.Read);

            SoapFormatter specialReader = new SoapFormatter();

            object obj = specialReader.Deserialize(fs);
            if (obj is Emp)
            {
                Emp e = (Emp)obj;
                Console.WriteLine(e.getDetails());
            }
            else
            {
                Console.WriteLine("Unknown type of data!!");
            }

            specialReader = null;
            fs.Flush();

            fs.Close();
            #endregion


        }
    }

    [Serializable]
    public class Emp
    {
        [NonSerialized]
        private string _Password = "mahesh@123";
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return No.ToString() + " - " + Name;
        }

    }
}
