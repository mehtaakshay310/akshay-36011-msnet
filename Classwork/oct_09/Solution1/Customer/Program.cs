﻿using Mathslib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Customer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter PAth of Dll or Exe created by .Net");
            string pathAssembly = Console.ReadLine();

            Assembly assembly = Assembly.LoadFrom(pathAssembly);

            Type[] types = assembly.GetTypes();
            foreach (Type type in types)
            {
                Console.WriteLine("__________________________________________________");
                Console.WriteLine("Name:"+type.FullName);

                MethodInfo[] methodInfo = type.GetMethods();

                foreach(MethodInfo method in methodInfo)
                {
                    Console.Write(method.ReturnType + " " + method.Name + "( ");

                    ParameterInfo[] parameterInfos = method.GetParameters();

                    foreach (ParameterInfo parameterInfo in parameterInfos)
                    {
                        Console.Write(parameterInfo.ParameterType.ToString() + " " + parameterInfo.Name + " ");
                    }
                    Console.WriteLine(" ) ");
                    Console.WriteLine();
                }
            }
        }
    }
}
