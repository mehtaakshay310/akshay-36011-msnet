﻿using mvcDemo.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvcDemo.Filter
{
    public class customFilter : ActionFilterAttribute
    {
        public override void  OnActionExecuted(ActionExecutedContext filterContext)
        {
            log.CreateLogger.PutLog(string.Format("/{0}/{1} is about to execute",
                filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                filterContext.ActionDescriptor.ActionName));
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            log.CreateLogger.PutLog(string.Format("/{0}/{1} executed",
                filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                filterContext.ActionDescriptor.ActionName));
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            log.CreateLogger.PutLog("UI proccessing is done");
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            log.CreateLogger.PutLog("UI process is about to begin");
        }
    }
}