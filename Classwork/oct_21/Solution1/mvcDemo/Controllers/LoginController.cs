﻿using mvcDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace mvcDemo.Controllers
{
    public class LoginController : Controller
    {
        SunbeamDBEntities dbObject = new SunbeamDBEntities();
        // GET: Login
        public ActionResult SignIn()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SignIn(HRLoginInfo hRLoginInfo, string ReturnUrl)
        {
            var count = (from hr in dbObject.HRLoginInfoes.ToList()
                         where hRLoginInfo.UserName.ToLower() == hr.UserName.ToLower()
                         && hRLoginInfo.Password == hr.Password
                         select hr).ToList().Count();

            if(count == 1)
            {
                FormsAuthentication.SetAuthCookie(hRLoginInfo.UserName, false);
                if(ReturnUrl!=null)
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return Redirect("/Home/Index");
                }
            }
            else
            {
                ViewBag.ErrorMessage = "Your Username and Password";
                return View();
            }
        }


        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Home/Index");
        }
    }
}