﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace mvcDemo.Helpers
{
    public class log
    {
        private static log loger = new log();
        private string FilePath = null;
        private FileStream fs = null;
        private StreamWriter writer = null;
        private log()
        {
            this.FilePath = ConfigurationManager.AppSettings["Logfile"].ToString();
        }

        public static log CreateLogger
        {
            get{ return loger; }
        }

        public void PutLog(string message)
        {
            try
            {
                fs = new FileStream(FilePath, FileMode.Append, FileAccess.Write);

                writer = new StreamWriter(fs);
                writer.WriteLine(string.Format("Log at {0}, details:{1}", DateTime.Now, message));
                writer.Flush();
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                writer.Close();
                fs.Close();
                writer = null;
                fs = null;
            }
        }
    }
}