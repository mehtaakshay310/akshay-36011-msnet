﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            #region database call
            //Database d = new Sql();
            //d.Insert();
            //d.Update();
            //d.Delete(); 
            #endregion

            #region Maths call
            A obj = new Maths();
            Console.WriteLine(obj.Add(10, 20));
            Console.WriteLine(obj.Sub(10, 20));

            B obj1 = new Maths();
            Console.WriteLine(obj1.Add(10, 20));

            #endregion
        }
    }

    #region Multiple Interface

    public interface A
    {
        int Add(int x, int y);
        int Sub(int x, int y);
    }

    public interface B
    {
        int Add(int x, int y);
        int Mult(int x, int y);
    }

    public class Maths : A, B
    {
        int A.Add(int x, int y)
        {
            return x + y;
        }

        int B.Add(int x, int y)
        {
            return x + y + 10; 
        }

        int B.Mult(int x, int y)
        {
            return x * y;
        }

        int A.Sub(int x, int y)
        {
            return x - y;
        }
    }
    #endregion

    #region Database interface implementation
    public interface Database
    {
        void Insert();
        void Update();
        void Delete();
    }

    public class Sql: Database
    {
        public void Insert()
        {
            Console.WriteLine("Sql Server Data Inserted");
        }
        public void Update()
        {
            Console.WriteLine("Sql Server Data Inserted");
        }
        public void Delete()
        {
            Console.WriteLine("Sql Server Data Inserted");
        }
    }

    public class Oracle: Database
    {
        public void Insert()
        {
            Console.WriteLine("oracle Server Data Inserted");
        }
        public void Update()
        {
            Console.WriteLine("oracle Server Data Inserted");
        }
        public void Delete()
        {
            Console.WriteLine("Oracle Server Data Inserted");
        }
    }
    #endregion


}
