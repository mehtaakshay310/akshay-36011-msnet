﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region integer Swap
            //Maths obj = new Maths();
            //int a = 10;
            //int b = 20;
            //Console.WriteLine("a="+a+"\tb="+b);
            //obj.Swap(ref a, ref b);
            //Console.WriteLine("a=" + a + "\tb=" + b); 
            #endregion

            #region Swap integer and Double
            //Maths<int> m1 = new Maths<int>();
            //int a = 10;
            //int b = 20;
            //Console.WriteLine("a=" + a + "\tb=" + b);
            //m1.Swap(ref a, ref b);
            //Console.WriteLine("a=" + a + "\tb=" + b);

            //Maths<double> m2 = new Maths<double>();
            //double a = 10;
            //double b = 20;
            //Console.WriteLine("a=" + a + "\tb=" + b);
            //m2.Swap(ref a, ref b);
            //Console.WriteLine("a=" + a + "\tb=" + b); 
            #endregion

            #region normal class with generic method
            //Maths m1 = new Maths();
            //int a = 10;
            //int b = 20;
            //Console.WriteLine("a=" + a + "\tb=" + b);
            //m1.Swap(ref a, ref b);
            //m1.add(a, b);
            //Console.WriteLine("a=" + a + "\tb=" + b); 
            #endregion

            #region inheriting generic class in normal class
            //specialMaths s = new specialMaths();
            //int a = 10;
            //int b = 20;
            //Console.WriteLine("a=" + a + "\tb=" + b);
            //s.Swap(ref a, ref b);
            //Console.WriteLine("a=" + a + "\tb=" + b); 
            #endregion

            #region Inheriting generic class in generic class
            //specialMaths<int,double,short,string > s = new specialMaths<int, double, short, string>();
            //short a = 10;
            //short b = 20;
            //Console.WriteLine( s.DoSomething(10,10.2,11,"abc"));
            //Console.WriteLine("a=" + a + "\tb=" + b);
            //s.Swap(ref a, ref b);
            //Console.WriteLine("a=" + a + "\tb=" + b); 
            #endregion
        }
    }

    #region Generic Class
    public class Maths<T>
    {
        public void Swap(ref T x, ref T y)
        {
            T temp = x;
            x = y;
            y = temp;
        }

        public void add(int x, int y)
        {
            Console.WriteLine(x + y);
        }
    }
    #endregion

    #region Normal Class with Generic Method
    public class Maths
    {
        public void Swap<T>(ref T x, ref T y)
        {
            T temp = x;
            x = y;
            y = temp;
        }

        public void add(int x, int y)
        {
            Console.WriteLine(x + y);
        }
    } 
    #endregion

    public class specialMaths<P,Q,R,S> : Maths<R>
    {
        public S DoSomething(P p,Q q,R r,S s)
        {
            return s;
        }
    }
}
