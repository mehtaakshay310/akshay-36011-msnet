﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericCollection
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Employee Objects
            Employee e1 = new Employee();
            e1.No = 1;
            e1.Name = "Akshay";

            Employee e2 = new Employee();
            e2.No = 2;
            e2.Name = "Shweta";

            Employee e3 = new Employee();
            e3.Name = "Amit";
            e3.No = 3;
            #endregion

            #region List
            //List<Employee> employees = new List<Employee>();
            //employees.Add(e1);
            //employees.Add(e2);
            //employees.Add(e3);

            //foreach (Employee employee in employees)
            //{
            //    Console.WriteLine(employee.getDetails());
            //} 
            #endregion

            #region List arrayList
            //List<object> list = new List<object>();
            //List<Utility<int>> utilities = new List<Utility<int>>(); 
            #endregion

            #region Stack
            //Stack<Employee> employees = new Stack<Employee>();
            //employees.Push(e1);
            //employees.Push(e2);
            //employees.Push(e3);
            //Console.WriteLine( employees.Peek());
            //employees.Pop();

            //foreach (Employee employee in employees) 
            //{
            //    Console.WriteLine(employee.getDetails());
            //} 
            #endregion

            #region Queue
            //Queue<Employee> employees = new Queue<Employee>();
            //employees.Enqueue(e1);
            //employees.Enqueue(e2);
            //employees.Enqueue(e3);

            //Employee e = employees.Dequeue();
            //Console.WriteLine(e);

            //foreach (Employee employee in employees)
            //{
            //    Console.WriteLine(employee);
            //} 
            #endregion

            #region Dictionary
            //Dictionary<int, Employee> employees = new Dictionary<int, Employee>();
            //employees.Add(1, e1);
            //employees.Add(2, e2);
            //employees.Add(3, e3);

            //Employee e = employees[3];
            //Console.WriteLine(e.getDetails());

            //foreach (int key in employees.Keys)
            //{
            //    Console.WriteLine("Key:" +key.ToString());
            //    Console.WriteLine(employees[key].getDetails());
            //} 
            #endregion

            Console.ReadLine();
        }
    }

    class Employee
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return this.No.ToString() +"\t"+ this.Name;
        }
    }

    class Utility<T>
    {

    }
}
