﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIleIO
{
    class Program
    {
        static void Main(string[] args)
        {
            #region File Writing
            //FileStream fs = new FileStream(@"D:\MsNet\workspace\akshay-36011-msnet\Classwork\oct_08\data.txt", FileMode.OpenOrCreate, FileAccess.Write);

            //StreamWriter writer = new StreamWriter(fs);

            //writer.WriteLine("Hello all");

            //writer.Close();
            //fs.Close();
            #endregion

            #region File Reading using read to end
            //FileStream fs = new FileStream(@"D:\MsNet\workspace\akshay-36011-msnet\Classwork\oct_08\data.txt", FileMode.Open, FileAccess.Read);

            //StreamReader reader = new StreamReader(fs);
            //Console.WriteLine(reader.ReadToEnd());
            #endregion

            #region File Reading using readline
            //FileStream fs = new FileStream(@"D:\MsNet\workspace\akshay-36011-msnet\Classwork\oct_08\data.txt", FileMode.Open, FileAccess.Read);

            //StreamReader reader = new StreamReader(fs);

            //while (true)
            //{
            //    string entireData = reader.ReadLine();
            //    if (entireData!=null)
            //    {
            //        Console.WriteLine(entireData);
            //    }
            //    else
            //    {
            //        break;
            //    }
            //}
            #endregion

            #region File Writing Employee
            Emp emp = new Emp();

            Console.WriteLine("Enter No:");
            emp.No = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Name");
            emp.Name = Console.ReadLine();


            FileStream fs = new FileStream(@"D:\MsNet\workspace\akshay-36011-msnet\Classwork\oct_08\data.txt", FileMode.OpenOrCreate, FileAccess.Write);


            StreamWriter writer = new StreamWriter(fs);

            writer.WriteLine(emp);

            fs.Flush();
            writer.Close();
            fs.Close();
            #endregion

        }
    }

    public class Emp
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return No.ToString() + ") " + Name;
        }

    }
}
