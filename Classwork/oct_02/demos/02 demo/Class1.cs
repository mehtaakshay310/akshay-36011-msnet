﻿using System;
using Maths;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_demo
{
    public class Class1
    {
        static void Main(string[] args)
        {
            string isContinue = "y";
            do
            {
                Console.WriteLine("Enter Your Choice");
                Console.WriteLine("1: Add, 2: Sub, 3:Mul, 4:Div");
                int choice = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter Value for X");
                int x = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter Value for Y");
                int y = Convert.ToInt32(Console.ReadLine());
                int result = 0;
                calc obj = new calc();
                switch (choice)
                {
                    case 1:
                        result = obj.Add(x,y);
                        break;
                    case 2:
                        result = obj.Sub(x,y);
                        break;
                    case 3:
                        result = obj.Mul(x,y);
                        break;
                    case 4:
                        result = obj.Div(x,y);
                        break;
                    default:
                        Console.WriteLine("Invalid Choice..Try Again!");
                        break;
                }
                Console.WriteLine(result);

                Console.WriteLine("Would you like to continue? y/n");
                isContinue = Console.ReadLine();
            } while (isContinue == "y");
            Console.WriteLine("Press ENTER to exit.......");
            Console.ReadLine();
        }
    }
}
