﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discontinued_Architecture
{
    class Program
    {
        static void Main(string[] args)
        {
            DataSet ds = new DataSet();

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=SunbeamDB;Integrated Security=True");

            SqlDataAdapter da = new SqlDataAdapter("Select * from Emp", con);
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            da.Fill(ds, "Employee");

            #region insert
            //SqlCommandBuilder builder = new SqlCommandBuilder(da);
            //DataRow row = ds.Tables["Employee"].NewRow();
            //row["Number"] = 12;
            //row["Name"] = "Jistesh";
            //row["Address"] = "Bhandup";
            //ds.Tables["Employee"].Rows.Add(row);
            //da.Update(ds, "Employee"); 
            #endregion

            #region Update
            //Console.WriteLine("Enter a No of Employee whose record you wish to update");
            //int no = Convert.ToInt32(Console.ReadLine());

            //DataRow refToTheRowToBeModified = ds.Tables[0].Rows.Find(no);
            //SqlCommandBuilder builder = new SqlCommandBuilder(da);

            //if (refToTheRowToBeModified != null)
            //{
            //    Console.WriteLine("EMployee FOund");
            //    Console.WriteLine(refToTheRowToBeModified["Name"] + " | " + refToTheRowToBeModified["Address"]);
            //    Console.WriteLine("---------------------------------------------------------------------");
            //    Console.WriteLine("Enter Name");
            //    refToTheRowToBeModified["Name"] = Console.ReadLine();
            //    Console.WriteLine("Enter Address");
            //    refToTheRowToBeModified["Address"] = Console.ReadLine();
            //    da.Update(ds, "Employee");
            //}
            //else
            //{
            //    Console.WriteLine("EMployee not found");
            //} 
            #endregion

            #region Delete
            Console.WriteLine("Enter a No of a Emp whose record you wish to Delete:");
            int no = Convert.ToInt32(Console.ReadLine());
            DataRow refToTheRowToBeDeleted = ds.Tables[0].Rows.Find(no);
            SqlCommandBuilder builder = new SqlCommandBuilder(da);

            if (refToTheRowToBeDeleted!=null)
            {
                Console.WriteLine("Data Found");
                Console.WriteLine(refToTheRowToBeDeleted["Name"] + " | " + refToTheRowToBeDeleted["Address"]);
                refToTheRowToBeDeleted.Delete();
                da.Update(ds, "Employee");
            }
            else
            {
                Console.WriteLine("Data Not Found");
            }
            #endregion
        }
    }
}
