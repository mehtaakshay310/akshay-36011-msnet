﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Data_Set
{
    class Program
    {
        static void Main(string[] args)
        {
            DataSet ds = new DataSet();
            DataTable table = new DataTable("Emp");

            DataColumn column1 = new DataColumn("Number", typeof(int));
            DataColumn column2 = new DataColumn("Name", typeof(string));
            DataColumn column3 = new DataColumn("Address", typeof(string));

            table.Columns.Add(column1);
            table.Columns.Add(column2);
            table.Columns.Add(column3);

            table.PrimaryKey =new  DataColumn[] { column1 };

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=SunbeamDB;Integrated Security=True");
            SqlCommand cmd = new SqlCommand("Select * from Emp",con);

            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            while(reader.Read())
            {
                DataRow row = table.NewRow();
                row["Number"] = Convert.ToInt32(reader["Number"]);
                row["Name"] = reader["Name"];
                row["Address"] = reader["Address"];
                table.Rows.Add(row);
            }
            con.Close();
            ds.Tables.Add(table);

            DataTable table1 = new DataTable("Books");
            DataColumn column4 = new DataColumn("ISBN", typeof(int));
            DataColumn column5 = new DataColumn("Name", typeof(string));

            table1.Columns.Add(column4);
            table1.Columns.Add(column5);

            DataRow row1 = table1.NewRow();
            row1["ISBN"] = 1;
            row1["Name"] = "The Secret";

            table1.Rows.Add(row1);
            ds.Tables.Add(table1);
        }
    }
}
