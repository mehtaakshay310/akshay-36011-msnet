﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace _00Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Sql Injection
            //string constr = ConfigurationManager.ConnectionStrings["connString"].ToString();
            //SqlConnection conn = new SqlConnection(constr);
            //Console.WriteLine("Enter Name");
            //string cmdText = "select count(*) from Emp where Name ='" + Console.ReadLine() + "'";
            //SqlCommand cmd = new SqlCommand(cmdText, conn);
            //conn.Open();

            //object result = cmd.ExecuteScalar();
            //int count = Convert.ToInt32(result);
            //if(count > 0)
            //{
            //    Console.WriteLine("You are Valid User");
            //}
            //else
            //{
            //    Console.WriteLine("You are invalid user!");
            //}

            //SqlDataReader reader = cmd.ExecuteReader();

            ////while(reader.Read())
            ////{
            ////    Console.WriteLine(string.Format("{0} | {1}", reader["Name"], reader["Address"]));
            ////}
            ////conn.Close(); 
            #endregion

            SqlConnection con = null;
            try
            {
                string constr = ConfigurationManager.ConnectionStrings["connString"].ToString();
                con = new SqlConnection(constr);
                SqlCommand cmd = new SqlCommand("spInsert", con);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter1 = new SqlParameter("@No", SqlDbType.Int);
                Console.WriteLine("Enter no");
                parameter1.Value = Convert.ToInt32(Console.ReadLine());

                SqlParameter parameter2 = new SqlParameter("@Name", SqlDbType.VarChar, 50);
                Console.WriteLine("Enter name");
                parameter2.Value = Console.ReadLine();

                SqlParameter parameter3 = new SqlParameter("@Address", SqlDbType.VarChar, 50);
                Console.WriteLine("Enter address");
                parameter3.Value = Console.ReadLine();

                cmd.Parameters.Add(parameter1);
                cmd.Parameters.Add(parameter2);
                cmd.Parameters.Add(parameter3);

                con.Open();
                cmd.ExecuteNonQuery();

                Console.WriteLine("Record Inserted using Stored Procedure....");
            }
            catch(Exception ex)
            {
                Console.WriteLine("Record Insertion Failed using Stored Procedure....");
                Console.WriteLine(ex);
            }
            finally
            {
                con.Close();
            }
        }
    }
}
