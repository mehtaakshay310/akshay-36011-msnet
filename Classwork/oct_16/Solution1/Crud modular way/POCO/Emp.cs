﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crud_modular_way.POCO
{
    public class Emp
    {
        public int Number { get; set; }
        public String Name { get; set; }
        public String Address { get; set; }
    }
}
