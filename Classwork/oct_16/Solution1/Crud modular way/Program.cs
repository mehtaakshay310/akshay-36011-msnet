﻿using Crud_modular_way.DAL;
using Crud_modular_way.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crud_modular_way
{
    class Program
    {
        static void Main(string[] args)
        {
            DBFactory dBFactory = new DBFactory();
            IDatabase database = dBFactory.GetDatabase();
            Console.WriteLine("Select Operation to be Perform");
            Console.WriteLine("1.Select\t2.Insert\t3.Update\t4.Delete");
            int choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    List<Emp> emps = database.select();
                    foreach (Emp emp in emps)
                    {
                        Console.WriteLine(emp.Number +" "+emp.Name+" "+emp.Address);
                    }
                    break;
                case 2:
                    Emp emp1 = new Emp();
                    Console.WriteLine("Enter Number");
                    emp1.Number = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("Enter Name");
                    emp1.Name = Console.ReadLine();


                    Console.WriteLine("Enter Address");
                    emp1.Address = Console.ReadLine();

                    int no=database.Insert(emp1);
                    Console.WriteLine("Number of Rows inserted = "+no);
                    break;
                case 3:
                    Emp emp2 = new Emp();
                    Console.WriteLine("Enter Number");
                    emp2.Number = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("Enter Name");
                    emp2.Name = Console.ReadLine();


                    Console.WriteLine("Enter Address");
                    emp2.Address = Console.ReadLine();

                    int no1 = database.Update(emp2);
                    Console.WriteLine("Number of Rows inserted = " + no1);
                    break;
                case 4:
                    Console.WriteLine("Enter Number");
                    int NO = Convert.ToInt32(Console.ReadLine());
                    int no2 = database.Delete(NO);
                    Console.WriteLine("Number of Rows inserted = " + no2);
                    break;
                default:
                    break;
            }
        }
    }
}
