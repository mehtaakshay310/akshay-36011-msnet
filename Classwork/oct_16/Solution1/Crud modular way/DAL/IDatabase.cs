﻿using Crud_modular_way.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crud_modular_way.DAL
{
    public interface IDatabase
    {
        List<Emp> select();
        int Insert(Emp emp);
        int Update(Emp emp);
        int Delete(int emp);
    }
}
