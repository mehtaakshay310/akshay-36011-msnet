﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Crud_modular_way.DAL
{
    public class DBFactory
    {
        public IDatabase GetDatabase()
        {
            int choice = Convert.ToInt32(ConfigurationManager.AppSettings["db"]);
            if(choice==1)
            {
                return  new SQLServer();
            }
            else
            {
                return new Oracle();
            }
        }  
    }
}
