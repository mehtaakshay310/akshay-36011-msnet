﻿using Crud_modular_way.POCO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crud_modular_way.DAL
{
    public class SQLServer : IDatabase
    {
        public int Delete(int No)
        {
            string constr = ConfigurationManager.ConnectionStrings["conString"].ToString();
            SqlConnection con = new SqlConnection(constr);
            string DeleteTemplate = "Delete from Emp where Number={0}";
            string query = string.Format(DeleteTemplate, No);
            SqlCommand cmd = new SqlCommand(query, con);

            con.Open();
            int rowsAffected = cmd.ExecuteNonQuery();
            con.Close();
            return rowsAffected;
        }

        public int Insert(Emp emp)
        {
            string constr = ConfigurationManager.ConnectionStrings["conString"].ToString();
            SqlConnection con = new SqlConnection(constr);
            string InsertTemplate = "Insert into Emp values({0},'{1}','{2}')";
            string query = string.Format(InsertTemplate, emp.Number, emp.Name, emp.Address);
            SqlCommand cmd = new SqlCommand(query, con);

            con.Open();
            int rowsAffected = cmd.ExecuteNonQuery();
            con.Close();
            return rowsAffected;
        }

        public List<Emp> select()
        {
            List<Emp> empList = new List<Emp>();

            string constr = ConfigurationManager.ConnectionStrings["conString"].ToString();
            SqlConnection con = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand("Select * from Emp", con);

            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            while(reader.Read())
            {
                Emp emp = new Emp() { Number = Convert.ToInt32(reader["Number"]),Address=reader["Address"].ToString(),Name=reader["Name"].ToString() };
                empList.Add(emp);
            }
            con.Close();
            return empList;
        }

        public int Update(Emp emp)
        {
            string constr = ConfigurationManager.ConnectionStrings["conString"].ToString();
            SqlConnection con = new SqlConnection(constr);
            string updateTemplate = "Update Emp set Name='{1}',Address='{2}' where Number = {0}";
            string query = string.Format(updateTemplate, emp.Number, emp.Name, emp.Address);
            SqlCommand cmd = new SqlCommand(query, con);

            con.Open();
            int rowsAffected = cmd.ExecuteNonQuery();
            con.Close();
            return rowsAffected;
        }
    }
}
