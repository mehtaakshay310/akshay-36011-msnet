﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace dtable
{
    class Program
    {
        static void Main(string[] args)
        {
            DataTable table = new DataTable("mytable");

            DataColumn column1 = new DataColumn("No", typeof(int));
            DataColumn column2 = new DataColumn("Name", typeof(string));
            DataColumn column3 = new DataColumn("Address", typeof(string));

            table.Columns.Add(column1);
            table.Columns.Add(column2);
            table.Columns.Add(column3);

            table.PrimaryKey = new DataColumn[] { column1 };

            DataRow r1 = table.NewRow();
            r1["No"] = 1;
            r1["Name"] = "Akshay";
            r1["Address"] = "Thane";

            DataRow r2 = table.NewRow();
            r2["No"] = 2;
            r2["Name"] = "Nimish";
            r2["Address"] = "Sydney";


            DataRow r3 = table.NewRow();
            r3["No"] = 3;
            r3["Name"] = "Vaibhav";
            r3["Address"] = "Thane";

            table.Rows.Add(r1);
            table.Rows.Add(r2);
            table.Rows.Add(r3);

        }
    }
}
