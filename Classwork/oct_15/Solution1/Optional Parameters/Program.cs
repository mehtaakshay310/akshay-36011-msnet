﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optional_Parameters
{
    class Program
    {
        static void Main(string[] args)
        {
            Player player1 = new Player();
            //string details = player1.GetDetails(10, "Akshay", "Thane");

            //string details = player1.GetDetails(10, "Sachin");
            //string details = player1.GetDetails(10);

            string details = player1.GetDetails(10, address: "Thane");

            Console.WriteLine(details);
        }
    }

    public class Player
    {
        public string GetDetails(int no, string name = "ABCD", string address = "Pune")
        {
            return string.Format("Details are: No is {0}. Your name is {1} and Address is {2}", no, name, address);

        }

        //public string GetDetails(int no, string name, string address)
        //{
        //    return string.Format("Details are: No is {0}. Your name is {1} and Address is {2}", no, name, address);

        //}
    }
}
