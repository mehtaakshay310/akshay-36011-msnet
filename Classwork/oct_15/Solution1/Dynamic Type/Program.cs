﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dynamic_Type
{
    class Program
    {
        static void Main(string[] args)
        {
            Factory factory = new Factory();
            Console.WriteLine("ENter choice as number");
            int choice = Convert.ToInt32(Console.ReadLine());

            dynamic obj = factory.GetMeSomeTypeObject(choice);
            Console.WriteLine(obj.GetDetails());

            //object obj = factory.GetMeSomeTypeObject(choice);

            //if (obj is Emp)
            //{
            //    Emp e = (Emp)obj;
            //    Console.WriteLine(e.GetDetails());
            //}
            //else if (obj is Book)
            //{
            //    Book b = (Book)obj;
            //    Console.WriteLine(b.GetBookDetails());
            //}
        }

    }
    public class Book
    {
        public string GetBookDetails()
        {
            return "Some book Details...";
        }
    }

    public class Emp
    {
        public string GetDetails()
        {
            return "Some emp Details...";
        }
    }

    public class Factory
    {
        public object GetMeSomeTypeObject(int choice)
        {
            if (choice == 1)
            {
                return new Emp();
            }
            else
            {
                return new Book();
            }
        }
    }

}

