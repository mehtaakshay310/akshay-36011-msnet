﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee e = new Employee();
            e.No = 1;
            e.Name = "akshay";
            e.Dname = "it";

            Customer c = new Customer();
            c.Name = "RUtank";
            c.No = 1;
            c.OrderDetails = "abc";

            Object obj = new Object();

            if (obj is int)
            {
                int j = Convert.ToInt32(obj);
                Console.WriteLine(j);
            }
            else if (obj is string)
            {
                string s = Convert.ToString(obj);
                Console.WriteLine(s);
            }
            else if (obj is Employee)
            {
                Employee e1 = (Employee)obj;
                Console.WriteLine(e1.getDetails());
                //Emp e2 = obj as Emp;
            }
            else if (obj is Customer)
            {
                Customer c1= (Customer)obj;
                Console.WriteLine(c1.getDetails());
            }
        }
    }
    class Employee
    {
        private int _No;
        private string _Name;
        private string _Dname;

        public string Dname
        {
            get { return _Dname; }
            set { _Dname = value; }
        }


        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return this.No.ToString() + this.Name + this.Dname.ToString();
        }
    }
    class Customer
    {
        private string _Name;
        private int _No;
        private string _Orderdetails;

        public string OrderDetails
        {
            get { return _Orderdetails; }
            set { _Orderdetails = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string getDetails()
        {
            return this.No.ToString() + this.Name + this.OrderDetails.ToString();
        }
    }
}
