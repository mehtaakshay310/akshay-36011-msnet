﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Emp e = new Emp();
            e.No = 100;
            e.Name = "mahesh";

            int i = 100;
            ///Object obj = new Object();
            object obj = i;
            obj = "abcd";
            obj = e;

            if (obj is int)
            {
                int j = Convert.ToInt32(obj);
                Console.WriteLine(j);
            }
            else if (obj is string)
            {
                string s = Convert.ToString(obj);
                Console.WriteLine(s);
            }
            else if (obj is Emp)
            {
                Emp e1 = (Emp)obj;
                Console.WriteLine(e1.getDetails());
                //Emp e2 = obj as Emp;
            }
        }
        public class Emp
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return this.No.ToString() + this.Name;
        }

    }
}
