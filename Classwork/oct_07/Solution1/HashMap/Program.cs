﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashMap
{
    class Program
    {
        static void Main(string[] args)
        {
            Emp e1 = new Emp();
            e1.No = 1;
            e1.Name = "Rajiv";

            Emp e2 = new Emp();
            e2.No = 2;
            e2.Name = "Rahul";

            Emp e3 = new Emp();
            e3.No = 3;
            e3.Name = "Mahesh";
            
            Hashtable arr = new Hashtable();
            arr.Add("a", e1);
            arr.Add("b", 100);
            arr.Add("c", "abcd");
            arr.Add("d", false);
            arr.Add("e", 10.2);
            arr.Add("f", new DateTime());
            arr.Add("g", new int[] { 10, 20, 30 });

            foreach (object keys in arr.Keys)
            {
                Console.WriteLine(keys);
                Console.WriteLine(arr[keys]);
            }

            Console.WriteLine("Enter Key");
            string key= Console.ReadLine();
            object obj =arr[key];
            
        }
    }
    public class Emp
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return this.No.ToString() + this.Name;
        }

    }
}
