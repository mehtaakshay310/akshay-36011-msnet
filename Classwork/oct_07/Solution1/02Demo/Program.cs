﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Simple Integer Array
            //int[] arr = new int[3];
            //arr[0] = 100;
            //arr[1] = 200;
            //arr[2] = 300;
            ////arr[3] = 400;

            //for (int i = 0; i < arr.Length; i++)
            //{
            //    Console.WriteLine(arr[i]);
            //}
            #endregion

            #region Simple Integer Array - II
            //int[] arr = new int[] { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };

            //for (int i = 0; i < arr.Length; i++)
            //{
            //    Console.WriteLine(arr[i]);
            //}
            #endregion

            #region Sample Employee Objects Used through out the demo
            Emp e1 = new Emp();
            e1.No = 1;
            e1.Name = "Akshay";

            Emp e2 = new Emp();
            e2.No = 2;
            e2.Name = "Vaibhav";

            Emp e3 = new Emp();
            e3.No = 3;
            e3.Name = "Rutank";
            #endregion

            #region Emp Array
            Emp[] employees = new Emp[3];
            employees[0] = e1;
            employees[1] = e2;
            employees[2] = e3;

            for (int i = 0; i < employees.Length; i++)
            {
                Emp e = employees[i];
                Console.WriteLine(e.getDetails());
            }
            #endregion

            #region Object Array of fixed size
            object[] arr = new object[5];
            arr[0] = e1;
            arr[1] = 100.1;
            arr[2] = "abcd";
            arr[3] = e2;
            arr[4] = false;

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] is int)
                {
                    
                       int k = Convert.ToInt32(arr[i]);//UnBoxing
                    Console.WriteLine(k);
                }
                else if (arr[i] is string)
                {
                    string s = Convert.ToString(arr[i]);
                    Console.WriteLine(s);
                }
                else if (arr[i] is Emp)
                {
                    Emp e = (Emp)arr[i];
                    Console.WriteLine(e.getDetails());
                }
                else if (arr[i] is bool)
                {
                    bool b = Convert.ToBoolean(arr[i]);//UnBoxing
                    Console.WriteLine(b);
                }
                else
                {
                    Console.WriteLine("unknown type of data!");
                }

            }
            #endregion
        }
    }

    public class Emp
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return this.No.ToString() + this.Name;
        }

    }
}
