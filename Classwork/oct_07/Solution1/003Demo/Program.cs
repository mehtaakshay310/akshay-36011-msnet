﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _003Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Emp e1 = new Emp();
            e1.No = 1;
            e1.Name = "Rajiv";

            Emp e2 = new Emp();
            e2.No = 2;
            e2.Name = "Rahul";

            Emp e3 = new Emp();
            e3.No = 3;
            e3.Name = "Mahesh";

            ArrayList arr = new ArrayList();
            arr.Add(e1);
            arr.Add(100);   //Boxing
            arr.Add("abcd");
            arr.Add(e2);
            arr.Add(false); //Boxing
            arr.Add(e3);
            arr.Add(10.2);
            arr.Add(new DateTime());
            arr.Add(new int[] { 10, 20, 30 });

            for (int i = 0; i < arr.Count; i++)
            {
                if (arr[i] is int)
                {
                    int k = Convert.ToInt32(arr[i]);
                    Console.WriteLine(k);
                }
                else if (arr[i] is string)
                {
                    string s = Convert.ToString(arr[i]);
                    Console.WriteLine(s);
                }
                else if (arr[i] is Emp)
                {
                    Emp e = (Emp)arr[i];
                    Console.WriteLine(e.getDetails());
                }
                else if (arr[i] is bool)
                {
                    bool b = Convert.ToBoolean(arr[i]);
                    Console.WriteLine(b);
                }
                else
                {
                    Console.WriteLine("unknown type of data!");
                }
            }
        }
    }
    public class Emp
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return this.No.ToString() + this.Name;
        }

    }
}
