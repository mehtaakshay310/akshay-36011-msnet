﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays_Task
{
    class Program
    {
        static void Main(string[] args)
        {
            Emp e1 = new Emp();
            e1.No = 1;
            e1.Name = "Akshay";
            e1.DName = "IT";

            Emp e2 = new Emp();
            e2.No = 2;
            e2.Name = "Shweta";
            e2.DName = "Admin";


            Emp e3 = new Emp();
            e3.No = 3;
            e3.Name = "Vaibhav";
            e3.DName = "IT";

            Customer c1 = new Customer();
            c1.No = 4;
            c1.Name = "Rutank";
            c1.OrderDetails = "Laptop";

            Customer c2 = new Customer();
            c2.No = 5;
            c2.Name = "Chinmay";
            c2.OrderDetails = "Mobile";

            Person[] people = new Person[5];
            people[0] = e1;
            people[1] = e2;
            people[2] = e3;
            people[3] = c1;
            people[4] = c2;

            for (int i = 0; i < people.Length; i++)
            {
                if (people[i] != null)
                {
                    Console.WriteLine(people[i].getDetails());
                }
            }
        }
    }

    public class Person
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }


        public virtual string getDetails()
        {
            return No.ToString() + Name;
        }
    }

    public class Emp : Person
    {
        private string _DName;

        public string DName
        {
            get { return _DName; }
            set { _DName = value; }
        }

        public override string getDetails()
        {
            return base.getDetails() + this.DName;
        }
    }

    public class Customer : Person
    {
        private string _OrderDetails;

        public string OrderDetails
        {
            get { return _OrderDetails; }
            set { _OrderDetails = value; }
        }
        public override string getDetails()
        {
            return base.getDetails() + this.OrderDetails;
        }
    }
}
