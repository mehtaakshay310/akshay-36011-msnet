﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array_Task_using_Object
{
    class Program
    {
        static void Main(string[] args)
        {
            Emp e1 = new Emp();
            e1.No = 1;
            e1.Name = "Akshay";
            e1.Dname = "IT";

            Emp e2 = new Emp();
            e2.No = 2;
            e2.Name = "Shweta";
            e2.Dname = "Admin";


            Emp e3 = new Emp();
            e3.No = 3;
            e3.Name = "Vaibhav";
            e3.Dname = "IT";

            Customer c1 = new Customer();
            c1.No = 4;
            c1.Name = "Dev";
            c1.OrderDetails = "Laptop";

            Customer c2 = new Customer();
            c2.No = 5;
            c2.Name = "Amol";
            c2.OrderDetails = "Mobile";

            object[] people = new object[5];
            people[0] = e1;
            people[1] = e2;
            people[2] = e3;
            people[3] = c1;
            people[4] = c2;

            for (int i = 0; i < people.Length; i++)
            {
                if (people[i] != null)
                {
                    if (people[i] is Emp)
                    {
                        Console.WriteLine(((Emp)people[i]).getDetails());
                    }
                    else if (people[i] is Customer)
                    {
                        Console.WriteLine(((Customer)people[i]).getDetails());
                    }
                }
            }
        }
    }
    public class Emp
    {
        private int _No;
        private string _Name;
        private string _Dname;

        public string Dname
        {
            get { return _Dname; }
            set { _Dname = value; }
        }


        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return this.No + this.Name + this.Dname;
        }

    }
    public class Customer
    {
        private int _No;
        private string _Name;
        private string _Details;

        public string OrderDetails
        {
            get { return _Details; }
            set { _Details = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return this.No + this.Name + this.OrderDetails;
        }
    }
}
