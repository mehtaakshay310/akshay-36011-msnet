﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            SunbeamDBEntities dbObject = new SunbeamDBEntities();

            #region Select using EF
            //var allEmps = dbObject.Emps.ToList();

            //foreach (var emp in allEmps)
            //{
            //    Console.WriteLine(emp.Name + " | " + emp.Address);
            //}
            #endregion

            #region Insert Using EF
            //dbObject.Emps.Add(
            //    new Emp() { Number = 20, Name = "Rutuja", Address = "Delhi" });

            //dbObject.SaveChanges();
            #endregion

            #region Update using EF
            //var empToModify = (from emp in dbObject.Emps.ToList()
            //                   where emp.Number == 20
            //                   select emp).First();

            //Console.WriteLine(empToModify.Name+" | "+empToModify.Address);

            //empToModify.Name = "changed";
            //empToModify.Address = "Pune";
            //dbObject.SaveChanges(); 
            #endregion

            #region Delete using EF
            //var empToBeDeleted = (from emp in dbObject.Emps.ToList()
            //                      where emp.Number == 20
            //                      select emp).First();

            //dbObject.Emps.Remove(empToBeDeleted);

            //dbObject.SaveChanges();
            //Console.WriteLine("Done");
            #endregion

            #region Stored Procedure CallUsing EF
            //dbObject.spInsert(99, "Rutank", "Pune");
            #endregion

            #region new table added
            var allBooks = dbObject.Books.ToList();

            foreach (var book in allBooks)
            {
                Console.WriteLine(book.ISBN + " | " + book.Titie);
            }

            #endregion
        }
    }
}
