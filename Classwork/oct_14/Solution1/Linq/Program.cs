﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Emp> emps = new List<Emp>()
            {
                new Emp{ No = 11, Name = "Akshay"  , Address="thane" },
                new Emp{ No = 12, Name = "Rahul"  , Address="panji" },
                new Emp{ No = 13, Name = "Rajiv"  , Address="mumbai" },
                new Emp{ No = 14, Name = "Ketan"  , Address="manglore" },
                new Emp{ No = 15, Name = "Yogesh"  , Address="banglore" },
                new Emp{ No = 16, Name = "Amit"  , Address="chennai" },
                new Emp{ No = 17, Name = "Nilesh"  , Address="pune" },
                new Emp{ No = 18, Name = "Nitin"  , Address="satara" },
                new Emp{ No = 19, Name = "Vishal"  , Address="kolhapur" },
                new Emp{ No = 20, Name = "Sachin"  , Address="chennai" }
            };
            Console.WriteLine("ENter CIty Search Character");
            string searchCharacter = Console.ReadLine();
            #region Filtering using foreach

            //List<Emp> result = new List<Emp>();
            //foreach (Emp emp in emps)
            //{
            //    if(emp.Address.StartsWith(searchCharacter))
            //    {
            //        result.Add(emp);
            //    }
            //}

            #endregion


            var result = (from emp in emps
                          where emp.Address.StartsWith(searchCharacter)
                          select new resultEmp() { EAddress=emp.Address, EName=emp.Name }
                          );

            emps.Add(new Emp { No = 21, Name = "Kishor", Address = "mulund" });

            foreach (var emp in result)
            {
                Console.WriteLine("Name : " + emp.EName+", Address : "+ emp.EAddress);
            }
        }
    }

    public class resultEmp
    {
        public string EName { get; set; }
        public string EAddress { get; set; }
    }

    class Emp
    {
        public string Address { get; set; }
        public string  Name { get; set; }
        public int No { get; set; }
    }
}
