﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sealed_Class
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public class A
    {
        public virtual void M1()
        {

        }
    }
    public class B : A
    {
        public sealed override void M1()
        {
            base.M1();
        }
    }

    public class C: B
    {
    }
}
