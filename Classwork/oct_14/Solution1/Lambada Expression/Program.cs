﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Linq.Expressions;

namespace Lambada_Expression
{
    delegate bool Mydelegate(int i);
    class Program
    {
        static void Main(string[] args)
        {
            //Func<int, bool> pointer = (i) => { return (i > 10); };
            //Mydelegate pointer =  (int i) =>
            //{
            //    return (i > 10);
            //};

            //stage 1:Create Expression Tree
            Expression<Func<int, bool>> tree = (i) => (i > 10);

            //stage 2:Compile Expression tree
            Func<int, bool> pointer = tree.Compile();

            Stopwatch watch = new Stopwatch();
            watch.Start();

            //bool result = Check(100); //631
            //bool result = pointer(100); //785
            //bool result = pointer(100); //974
            //bool result = pointer(100); //974
            //bool result = pointer(100); //1500
            //bool result = pointer(100); //636
            bool result = pointer(100); //36


            watch.Stop();
            Console.WriteLine("Time Taken = "+ watch.ElapsedTicks.ToString());
            Console.WriteLine("Result is " + result);

            Console.ReadLine();
        }

        //public static bool Check(int i)
        //{
        //    return (i > 10);
        //}
    }
}
