﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extension_Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter some data");
            string data = Console.ReadLine();

            //MyUtilityClass obj = new MyUtilityClass();
            //bool result = obj.CheckForValidEMailAddress(data,10);

            //bool result = MyUtilityClass.CheckForValidEMailAddress(data, 100);

            bool result = data.CheckForValidEMailAddress(100);

            Console.WriteLine(result);
            
            int[] arr = new int[] { 10, 20, 30, 40, 50, 60 };
            Console.WriteLine(arr.Average());

            var v = new { No = 1, Name = "Akshay", Address = "Thane" };
            v.CheckForValidEMailAddress(8);
        }
    }

    public static class MyUtilityClass
    {
        public static bool CheckForValidEMailAddress<T>(this T str, int i)
        {
            return true;
        }
        public static bool CheckForValidEMailAddress(this string str, int i)
        {
            return str.Contains("@");
        }
    }
}
