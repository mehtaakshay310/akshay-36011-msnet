﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Maths m = new Maths();
            Console.WriteLine("Enter 1st Integer");
            int x = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter 2nt Integer");
            int y = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter 3rd Integer");
            int z = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(m.Add(x,y));
            Console.WriteLine(m.Add(x,y,z));
        }
    }
    public class Maths
    {
        public int Add(int x,int y)
        {
            return x + y;
        }

        public int Add(int x, int y,int z)
        {
            return x + y + z;
        }
    }
}
