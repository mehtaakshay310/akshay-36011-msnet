﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Main Same like C++
            Person p = new Person();
            p.Name="Akshay";
            p.Age=22;
            Console.WriteLine(p.GetDetails());
            #endregion
        }
    }

    public class Person
    {
        private string _Name;
        private int _Age;

        #region Constructor of Person
        public Person()
        {
            this.Name="";
            this.Age=0;
        }

        #endregion

        #region Getters Setters

        public string Name
        {
            get {
                return "Mr/Mrs " + this._Name;
            }
            set {
                if (value == "")
                {
                    this._Name = "No Data";
                }
                else
                {
                    this._Name = value;
                }
            }
        }

        public int Age
        {
            get {
                return this._Age;
            }

            set
            {
                this._Age = value;
            }
        }

        #endregion

        #region Facilitator
        public string GetDetails()
        {
            return "Welcome " + this.Name + " has age " + this.Age.ToString();
        }

        #endregion
    }
}
