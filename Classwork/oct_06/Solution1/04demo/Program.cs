﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04demo
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Logger.CurrentLogger.Log("Main Has Been Called.....");
            
                Employee e = new Employee();
                e.getDetails();

                Maths m = new Maths();

                Console.WriteLine("Enter First Number");
                int x = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter Second Number");
                int y = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine( m.add(x, y));

            }
            catch(Exception ex)
            {
                Logger.CurrentLogger.Log(ex.Message);
            }
        }
    }

    class Employee
    {
        public void getDetails()
        {
            Logger.CurrentLogger.Log("Emoloyee get details is called");
        }
    }

    class Maths
    {
        public int add(int x, int y)
        {
            Logger.CurrentLogger.Log("Maths add is called having x = " + x + ",y= " + y);
            return x + y;
        }
    }

    class Logger
    {
        private static Logger _logger = new Logger();
        
        private Logger()
        {
            Console.WriteLine("Logger object Created");
        }

        public static Logger CurrentLogger
        {
            get
            {
                return _logger;
            }
        }

        public void Log(string msg)
        {
            Console.WriteLine("Logged "+ msg +" @ "+ DateTime.Now.ToString());
        }
    }
}
