﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Main Same like C++
            Person p = new Person();
            p.Set_Name("Akshay");
            p.Set_Age(22);
            Console.WriteLine(p.GetDetails());
            #endregion
        }
    }

    public class Person
    {
        private string _Name;
        private int _Age;

        #region Constructor of Person
        public Person()
        {
            this.Set_Name("");
            this.Set_Age(0);
        }

        #endregion

        #region Getters Setters
        public string Get_Name()
        {
            return "Mr/Mrs "+this._Name;
        }

        public int Get_Age()
        {
            return this._Age;
        }

        /// <summary>
        /// this method is used for setting age
        /// </summary>
        /// <param name="age">age is used to set age</param>
        public void Set_Age(int age)
        {
            this._Age = age;
        }

        public void Set_Name(string name)
        {
            if(name=="")
            {
                this._Name = "No Data";
            }
            else
            {
                this._Name = name;
            }
        }

        #endregion

        #region Facilitator
        public string GetDetails()
        {
            return "Welcome " + this.Get_Name() + " has age " + this.Get_Age();
        }

        #endregion
    }
}
