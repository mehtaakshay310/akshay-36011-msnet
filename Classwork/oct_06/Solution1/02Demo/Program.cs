﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Main Same like C++
            ConsultantEmployee e = new ConsultantEmployee();                
            e.Name = "Akshay";
            e.Age = 22;
            e.Dname = "Devops";
            e.WorkingHours = 10;
            Console.WriteLine(e.GetDetails());
            #endregion
        }
    }

    public class Person
    {
        private string _Name;
        private int _Age;

        #region Constructor of Person
        public Person()
        {
            this.Name = "";
            this.Age = 0;
        }

        #endregion

        #region Getters Setters

        public string Name
        {
            get
            {
                return "Mr/Mrs " + this._Name;
            }
            set
            {
                if (value == "")
                {
                    this._Name = "No Data";
                }
                else
                {
                    this._Name = value;
                }
            }
        }

        public int Age
        {
            get
            {
                return this._Age;
            }

            set
            {
                this._Age = value;
            }
        }

        #endregion

        #region Facilitator
        public virtual string GetDetails()
        {
            return "Welcome " + this.Name + " has age " + this.Age.ToString();
        }

        #endregion
    }

    class Employee: Person
    {
        private string _Dname;

        public string Dname
        {
            get { return _Dname; }
            set { _Dname = value; }
        }

        public override string GetDetails()
        {
            return base.GetDetails() + " and his Department name is "+this.Dname ;
        }
    }

    class ConsultantEmployee:Employee
    {
        private int _WorkingHours;

        public int WorkingHours
        {
            get { return _WorkingHours; }
            set { _WorkingHours = value; }
        }
        public override string GetDetails()
        {
            return base.GetDetails()+" hs working hours "+ this.WorkingHours;
        }
    }
}
