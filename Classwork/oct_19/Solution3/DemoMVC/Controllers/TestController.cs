﻿using DemoMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoMVC.Controllers
{
    
    public class TestController : Controller
    {
        SunbeamDBEntities1 dbObj = new SunbeamDBEntities1();
        // GET: Test
        #region List of Hardcoded Emp
        //public ActionResult Show()
        //{
        //    List<Emp> emps = new List<Emp>()
        //    {
        //        new Emp() { Name = "Akshay1", Address = "Thane1", Number = 1 },
        //        new Emp() { Name = "Akshay2", Address = "Thane2", Number = 2 },
        //        new Emp() { Name = "Akshay3", Address = "Thane3", Number = 3 },
        //        new Emp() { Name = "Akshay4", Address = "Thane4", Number = 4 },
        //    };
        //    return View(emps);
        //} 
        #endregion

        public ActionResult Show()
        {
            try
            {
            var allEmps = dbObj.Emps.ToList();
            return View(allEmps);

            }
            catch (Exception e)
            {
                return View("Error",e);
            }
        }

        public ActionResult Update(int id)
        {
            try
            {
            var empTobeUpdated = (from emp in dbObj.Emps.ToList()
                                  where emp.Number == id
                                  select emp).First();
            return View(empTobeUpdated);

            }
            catch (Exception e)
            {
                return View("Error",e);
            }
        }

        public ActionResult AfterUpdate(FormCollection formCollection)
        {
            try
            {
            var empTobeUpdated = (from emp in dbObj.Emps.ToList()
                                  where emp.Number == Convert.ToInt32(formCollection["Number"])
                                  select emp).First();
            empTobeUpdated.Name = formCollection["Name"];
            empTobeUpdated.Address = formCollection["Address"];

            dbObj.SaveChanges();

            return Redirect("/Test/Show");

            }
            catch (Exception e)
            {
                return View("Error",e);
            }

        }

        public ActionResult Delete(int id)
        {
            try
            {
            var emptobedeleted = (from emp in dbObj.Emps.ToList()
                                  where emp.Number ==id
                                  select emp).First();

            dbObj.Emps.Remove(emptobedeleted);
            dbObj.SaveChanges();

            return Redirect("/Test/Show");

            }
            catch (Exception e)
            {
                return View("Error",e);
            }
        }

        public ActionResult Insert()
        {
            try
            {
            return View();

            }
            catch (Exception e)
            {
                return View("Error",e);
            }
        }

        public ActionResult AfterInsert(FormCollection formCollection)
        {
            try
            {
            Emp emp = new Emp()
            {
                Number = Convert.ToInt32(formCollection["Number"]),
                Name = formCollection["Name"].ToString(),
                Address = formCollection["Address"].ToString()
            };
            dbObj.Emps.Add(emp);
            dbObj.SaveChanges();
            return Redirect("/Test/Show");

            }
            catch (Exception e)
            {
                return View("Error",e);
            }
        }
    }
}