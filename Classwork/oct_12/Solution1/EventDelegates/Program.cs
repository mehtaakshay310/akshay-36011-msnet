﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB;

namespace EventDelegates
{
    class Program
    {
        static void Main(string[] args)
        {
            MySqlServer mySqlServer = new MySqlServer();

            mydelegate onInsert = new mydelegate(OnInsertCallMe);
            mydelegate onUpdate = new mydelegate(OnUpdateCallMe);
            mydelegate onDelete = new mydelegate(OnDeleteCallMe);

            mySqlServer.inserted += onInsert;
            mySqlServer.updated += onUpdate;
            mySqlServer.deleted += onDelete;

            mySqlServer.insert("hello");
            mySqlServer.update("akshay");
            mySqlServer.delete("abc");
        }

        public static void OnInsertCallMe()
        {
            Console.WriteLine("Logging Insert into Console");
        }

        public static void OnUpdateCallMe()
        {
            Console.WriteLine("Logging Update into Console");
        }

        public static void OnDeleteCallMe()
        {
            Console.WriteLine("Logging Delete into Console");
        }
    }
}

namespace DB
{

    public delegate void mydelegate();

    public class MySqlServer
    {
        public event mydelegate inserted;
        public event mydelegate updated;
        public event mydelegate deleted;

        public void insert(string data)
        {
            Console.WriteLine(data+" Inserted");
            inserted();
        }

        public void update(string data)
        {
            Console.WriteLine(data + " Updated");
            updated();
        }

        public void delete(string data)
        {
            Console.WriteLine(data + " Deleted");
            deleted();
        }
    }
}
