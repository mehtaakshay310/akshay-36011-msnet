﻿using myOwnAttribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POCO
{
    [Table(Name = "Employee")]
    public class Emp
    {
        private int _Eno;
        private string _Ename;

        [Column(Name ="Ename",Type ="varchar(50)")]
        public string Ename
        {
            get { return _Ename; }
            set { _Ename = value; }
        }

        [Column(Name ="Eno",Type ="int")]
        public int ENo
        {
            get { return _Eno; }
            set { _Eno = value; }
        }

    }

    [Table(Name = "Book")]
    public class Book
    {
       
        private int _Isbn;
        [Column(Name ="isbn",Type ="int")]
        public int ISBN
        {
            get { return _Isbn; }
            set { _Isbn = value; }
        }

        private string _Title;

        [Column(Name ="Title",Type ="varchar(50)")]
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }


    }
}
