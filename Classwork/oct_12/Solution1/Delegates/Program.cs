﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    delegate int mydelegate(int p, int q);
    delegate void myhellodelegate(string name);
    class Program
    {
        static void Main(string[] args)
        {
            mydelegate mydelegate = new mydelegate(sub);
            Console.WriteLine("Result = " +mydelegate(10, 20));

            myhellodelegate myhellodelegate = new myhellodelegate(hello);
            myhellodelegate("akshay");
        }

        static void hello(string name)
        {
            Console.WriteLine("hello"+name);
        }
        static int add(int p,int q)
        {
            return p + q;
        }

        static int sub(int p, int q)
        {
            return p - q;
        }
    }
}
