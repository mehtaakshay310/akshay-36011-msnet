﻿using myOwnAttribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DbGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Path of dll or exe");
            string pathOfAssembly = Console.ReadLine();
            Assembly assembly = Assembly.LoadFrom(pathOfAssembly);
            Type[] allTypes = assembly.GetTypes();
            string query = "";

            foreach (Type type in allTypes)
            {
                Console.WriteLine("Type: "+type.Name);
                List<Attribute> allAttributes = type.GetCustomAttributes().ToList();
                foreach (Attribute attribute in allAttributes)
                {
                    if(attribute is Table)
                    {
                        Table table = (Table)attribute;
                        query += "create table "+table.Name+" (";
                        break;
                    }
                }

                PropertyInfo[] allGettersSetters = type.GetProperties();

                foreach (PropertyInfo propertyInfo in allGettersSetters)
                {
                    List<Attribute> allAttributesProperties = propertyInfo.GetCustomAttributes().ToList();
                    foreach (Attribute attribute in allAttributesProperties)
                    {
                        if (attribute is Column)
                        {
                            Column column = (Column)attribute;
                            query += column.Name + " " + column.Type + ",";
                            break;
                        }
                    }
                }
                query = query.TrimEnd(new char[] { ',' });
                query = query + " ); ";
            }
                Console.WriteLine(query);
        }
    }
}
