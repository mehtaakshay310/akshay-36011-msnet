﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter path of dll or exe");
            string pathOfAssembly = Console.ReadLine();
            Assembly assembly = Assembly.LoadFrom(pathOfAssembly);
            Type[] allTypes = assembly.GetTypes();
            object dynamicObject = null;
            foreach (Type type in allTypes)
            {
                Console.WriteLine("Type:"+type.Name);
                dynamicObject = assembly.CreateInstance(type.FullName);
                MethodInfo[] methodInfos = type.GetMethods(BindingFlags.Public |
                        BindingFlags.Instance |
                        BindingFlags.DeclaredOnly);
                foreach (MethodInfo method in methodInfos)
                {
                    Console.WriteLine("Calling method "+method.Name+"(");
                    ParameterInfo[] parameterInfos = method.GetParameters();
                    foreach (ParameterInfo parameterInfo  in parameterInfos)
                    {
                        Console.Write(parameterInfo.ParameterType+" "+parameterInfo.Name+" ");
                    }
                    Console.Write(");\n");
                    object[] parameters = new object[] { 10,20 };
                    object result = type.InvokeMember(method.Name,
                        BindingFlags.Public |
                        BindingFlags.Instance |
                        BindingFlags.InvokeMethod,
                        null, dynamicObject, parameters);
                    Console.WriteLine("Result of "+ method.Name+"executed is"+result.ToString());
                }
            }
        }
    }
}
