﻿using Second;
using Second.third;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Maths m = new Maths();
            Console.WriteLine(m.Add(10,20));

            test t = new test();
            Console.WriteLine(t.Sample());

            DB dbObj = new DB();
            dbObj.Insert();

            Console.ReadLine();
        }
    }
}

namespace Second
{
    public class Maths
    {
        public int Add(int x, int y)
        {
            return x + y;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("This is MAIN method in Maths class");
        }
    }

    public class test
    {
        public string Sample()
        {
            return "this is test";
        }
    }

    namespace third
    {
        public class DB
        {
            static void Main(string[] args)
            {
                Console.WriteLine("This is MAIN method in DB class");
                Console.ReadLine();
            }

            public void Insert()
            {
                Console.WriteLine("Data Inserted Successfully");
            }
        }
    }
}