﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Report Type Needed:");
            Console.WriteLine("1: PDF, 2: Excel, 3: Word");
            int choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Pdf();
                    break;
                case 2:
                    Excel();
                    break;
                case 3:
                    Word();
                    break;
                default:
                    Console.WriteLine("Invalid Report Selected");
                    break;
            }
            Console.ReadLine();
        }

        public static void Pdf()
        {
            Console.WriteLine("Pdf Data Parsed");
            Console.WriteLine("Pdf Data Validated");
            Console.WriteLine("Pdf Data Saved");
        }

        public static void Excel()
        {
            Console.WriteLine("Excel Data Parsed");
            Console.WriteLine("Excel Data Validated");
            Console.WriteLine("Excel Data Saved");
        }

        public static void Word()
        {
            Console.WriteLine("Word Data Parsed");
            Console.WriteLine("Word Data Validated");
            Console.WriteLine("Word Data Saved");
        }
    }
}
