﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _003demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Report Type Needed:");
            Console.WriteLine("1: PDF, 2: Excel, 3: Word, 4: Text");
            int choice = Convert.ToInt32(Console.ReadLine());
            ReportFactory rp = new ReportFactory();
            Result obj = rp.GetReport(choice);
            obj.generate();
            Console.ReadLine();
        }
    }

    public class ReportFactory
    {
        public Result GetReport(int choice)
        {
            if(choice ==1)
            {
                return new Pdf();
            }

            else if(choice == 2)
            {
                return new Excel();
            }

            else if(choice == 3)
            {
                return new Word();
            }

            else
            {
                return new Text();
            }
        }
    }

    public abstract class Result
    {
        protected abstract void parse();

        protected abstract void validate();

        protected abstract void save();

        public virtual void generate()
        {
            parse();
            validate();
            save();
        }
    }

    public class Pdf : Result
    {
        protected override void parse()
        {
            Console.WriteLine("Pdf Data Parsed");
        }

        protected override void validate()
        {
            Console.WriteLine("Pdf Data Validated");
        }

        protected override void save()
        {
            Console.WriteLine("Pdf Data Saved");
        }
    }

    public class Excel : Result
    {
        protected override void parse()
        {
            Console.WriteLine("Excel Data Parsed");
        }

        protected override void validate()
        {
            Console.WriteLine("Excel Data Validated");
        }

        protected override void save()
        {
            Console.WriteLine("Excel Data Saved");
        }
    }

    public class Word : Result
    {
        protected override void parse()
        {
            Console.WriteLine("Word Data Parsed");
        }

        protected override void validate()
        {
            Console.WriteLine("Word Data Validated");
        }

        protected override void save()
        {
            Console.WriteLine("Word Data Saved");
        }
    }

    public class Text : Result
    {
        protected override void parse()
        {
            Console.WriteLine("Text Data Parsed");
        }

        protected override void save()
        {
            Console.WriteLine("Text Data Saved");
        }

        protected override void validate()
        {
            Console.WriteLine("Text Data Validated");
        }

        protected void revalidate()
        {
            Console.WriteLine("Text Data Revalidated");
        }

        public override void generate()
        {
            parse();
            validate();
            revalidate();
            save();
        }
    }
}
