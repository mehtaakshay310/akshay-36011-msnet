﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02Demo_oop_manner_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Report Type Needed:");
            Console.WriteLine("1: PDF, 2: Excel, 3: Word");
            int choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Pdf obj1 = new Pdf();
                    obj1.generate();
                    break;
                case 2:
                    Excel obj2 = new Excel();
                    obj2.generate();
                    break;
                case 3:
                    Word obj3 = new Word();
                    obj3.generate(); 
                    break;
                case 4:
                    Text obj4 = new Text();
                    obj4.generate();
                    break;
                default:
                    Console.WriteLine("Invalid Report Selected");
                    break;
            }
            Console.ReadLine();
        }
    }

    public abstract class Result
    {
        protected abstract void parse();

        protected abstract void validate();

        protected abstract void save();

        public virtual void generate()
        {
            parse();
            validate();
            save();
        }
    }

    public class Pdf: Result
    {
        protected override void parse()
        {
            Console.WriteLine("Pdf Data Parsed");
        }

        protected override void validate()
        {
            Console.WriteLine("Pdf Data Validated");
        }

        protected override void save()
        {
            Console.WriteLine("Pdf Data Saved");
        }
    }

    public class Excel: Result
    {
        protected override void parse()
        {
            Console.WriteLine("Excel Data Parsed");
        }

        protected override void validate()
        {
            Console.WriteLine("Excel Data Validated");
        }

        protected override void save()
        {
            Console.WriteLine("Excel Data Saved");
        }
    }

    public class Word: Result
    {
        protected override void parse()
        {
            Console.WriteLine("Word Data Parsed");
        }

        protected override void validate()
        {
            Console.WriteLine("Word Data Validated");
        }

        protected override void save()
        {
            Console.WriteLine("Word Data Saved");
        }
    }

    public class Text : Result
    {
        protected override void parse()
        {
            Console.WriteLine("Text Data Parsed");
        }

        protected override void save()
        {
            Console.WriteLine("Text Data Saved");
        }

        protected override void validate()
        {
            Console.WriteLine("Text Data Validated");
        }

        protected void revalidate()
        {
            Console.WriteLine("Text Data Revalidated");
        }

        public override void generate()
        {
            parse();
            validate();
            revalidate();
            save();
        }
    }
}
