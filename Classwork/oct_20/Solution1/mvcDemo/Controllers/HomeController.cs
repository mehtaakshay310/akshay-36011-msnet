﻿using mvcDemo.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace mvcDemo.Controllers
{
    public class HomeController : Controller
    {
        SunbeamDBEntities dbObject = new SunbeamDBEntities(); 
        // GET: Home
        public ActionResult Index()
        {
            try
            {
                ViewData["message"] = "Welcome to my app";
                List<Emp> allemps = dbObject.Emps.ToList();
                return View(allemps);
            }
            catch (Exception ex)
            {
                return View("Error",ex);
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Emp empToBeEdited = (from emp in dbObject.Emps.ToList()
                       where emp.Number == id
                       select emp
                       ).First();
            ViewBag.message = "Employee to Be added "+empToBeEdited.Name;
            return View(empToBeEdited);
        }

        [HttpPost]
        public ActionResult Edit(Emp empUpdate)
        {
            Emp empToBeUpdate = (from emp in dbObject.Emps.ToList()
                                 where emp.Number == empUpdate.Number
                                 select emp
                       ).First();
            empToBeUpdate.Name = empUpdate.Name;
            empToBeUpdate.Address = empUpdate.Address;
            dbObject.SaveChanges();
            return Redirect("/Home/Index");
        }

        public ActionResult Delete(int id)
        {
            Emp empToBeDeleted = (from emp in dbObject.Emps.ToList()
                                 where emp.Number == id
                                 select emp
                       ).First();
            dbObject.Emps.Remove(empToBeDeleted);
            dbObject.SaveChanges();
            return Redirect("/Home/Index");
        }

        public ActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Add(Emp empAdded)
        {
            dbObject.Emps.Add(empAdded);
            dbObject.SaveChanges();
            return Redirect("/Home/Index");
        }
        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Contact(Contact contact)
        {
            try
            {
                string emailUserName = ConfigurationManager.AppSettings["email"];
                string emailPassword = ConfigurationManager.AppSettings["password"];

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(emailUserName);
                mail.To.Add(contact.Email);
                mail.CC.Add("mehtaakshay310@gmail.com");

                mail.Subject = "New Query Recieved......!!!!!";
                mail.Body = "<h4>" + contact.ToString() + "<h4>";
                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);

                smtp.Credentials = new NetworkCredential(emailUserName, emailPassword);
                smtp.EnableSsl = true;

                smtp.Send(mail);
                ViewBag.message = "Your Query submitted Successfully";

                return View();
            }
            catch (Exception ex)
            {
                ViewBag.message = "Some Error" + ex.Message;
                return View();
            }
        }
    }
}