﻿using mvcDemo.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace mvcDemo.Models
{
    [MetadataType(typeof(extraInfoAboutEmp))]
    public partial class Emp
    {
    }

    public class extraInfoAboutEmp
    {
        [Required(ErrorMessage ="Number Cannot Be Null")]
        [Range(1,100,ErrorMessage ="Range of Number should be between 1 to 100")]
        public int Number { get; set; }

        [SbValidator(ErrorMessage ="Name cannot be 1234")]
        [Required(ErrorMessage = "Name Cannot Be Null")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Address Cannot Be Null")]
        public string Address { get; set; }
    }
}